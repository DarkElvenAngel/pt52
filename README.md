# Raspberry Pi Pico - PT52 or Pico Terminal 52

**PT52** is a VT52 Clone, but it's so much more than that.  This is using a [VGA demo base](https://shop.pimoroni.com/products/pimoroni-pico-vga-demo-base) from Pimoroni.

## What is in a name

PT52 - Pico Terminal vt52 compatible.  *Stage 1* **COMPLETE**  
PT52E - Pico Terminal Enhanced 256 colour *Stage 2* **COMPLETE**  
PT52E - Pico Terminal Enhanced with SD-Card support *Stage 3* **COMPLETE**  
PT52E - Pico Terminal Enhanced with C Interpreter *Stage 3* **COMPLETE**  
PT52EG - Pico Terminal Enhanced Graphics support *Stage ?*  
More coming

## Setup

In the default configuration the PT52E is configured to 80 column x 24 line with 256 colours in a 640x480 resolution this is scaled to 640x240.  You will need a Demo Board with the pico installed, also needed are headers installed at the **SD JUMPERS** on the GPIO side is good enough.  These pins become the serial port, You'll also need a USB micro B to USB A adapter and a USB Keyboard *\**

NOTE that putting a jumper from GPIO21 to GPIO20 will Loop back the terminal.

*\** Keyboard support isn't the best this is due to the tinyUSB lib it doesn't seem to like newer, or wireless keyboards. I use a DELL SK-8115 that seems to work fine.

To adjust settings it's best to make changes in config.h there are some safeguards in place if settings are out of range.

## Changes with PT52E

256 Colour mode is now default. A simple command interpreter has been added to make a simple shell. Serial UART is inactive at bootup and only brought up when the terminal app is started. Self tests have been disabled. Boot up is quick into the **ptsh**. There are some commands that do nothing at this time or simply don't work This is a work in progress! The PT52E has SD card support *current version*.

I've updated the code to run with SDK version 1.3 this fixes some problems with the USB stack.

## PT52 SHELL

The shell is extremely simple and only has a few commands.

* **help** - List all available commands and their function  
* **exit** - does nothing  
* **iceberg** - GAME translated from BASIC to C  
* **test** - This is more of a placeholder or testbed for new function testing  
* **cls** - Clear screen  
* **mem** - Display the largest available memory block  
* **term** - The Serial Terminal  
* **basic** - uBasic by Adam Dunkels  
* **hex** - Hex viewer by Ivaylo Stoyanov  
* **edit** - Kilo text editor by Salvatore Sanfilippo  
* **run** - pcc C Interpreter by Patrick Wu  

### PT52 disk/file commands

* **cd** - Change directory  
* **ls** - list directory  
* **type** - Type contents of file  
* **mkdir** - Make directory  
* **del** - Delete file of directory  
* **move** - Move or rename file  
* **copy** - Copy file  

### ICEBERG

Your hull is badly damaged and you've no weapons to speak of. As you limp slowly home through  treacherous iceberg-strew waters, you become aware that an enemy ship is tailing you.  Strangely it can detect you but not the icebergs, your best chance is to lure them in hitting one.

This simple text mode game was ported from BASIC the controls are ( N,S,E,W) you control the 'Y' ship your Enemy is the 'Z' the '*' are the Icebergs.  Not all games are winnable.

### ~~BASIC~~

~~This is a port of uBasic by Adam Dunkels it's a very limited and simple BASIC interpreter. Mostly this is a proof of concept and a place holder for something more advanced.  Currently there is no way to break out of a program and there aren't any error messages.~~  I've disabled BASIC in this revision.

### TERM

The serial terminal function is now moved to an application.  It functions the same as before with the exception of the **MENU** Key press this to open the terminal menu and from here you can quit back to shell. More features are planned but not yet complete.

### HEX

The hex viewer can be used to explore the Pico's memory and storage.  This application is not entirely complete and it's possible to crash the system if you page too far.  This is a known limitation that I haven't fixed.

### C interpreter

pcc by Patrick Wu, has been ported to the pt52 and can run simple C programs.

### EDIT

kilo text editor by Salvatore Sanfilippo is included as a general purpose text editor. I've added line numbering and working on so more features this will become the IDE for any script languages I have included.

## The power of C

With the C interpreter working you can now write programs on the PT52 and run them from ptsh by simply typing the file name. 

## More to come

I'm still working to improve this project and I hope you find it useful and or educational.  
~~Looking to add some menu support to access things like serial setting or run little games or programs.~~ **COMPLETED**  
Some character RAM for custom characters, this could be used for simple animations.  
When the screen isn't fully in use with the text buffer status or UI bars in the unused areas.  
And more if it will fit!

## How to use PT52

Once setup hook up to a Raspberry Pi for example power up the PT52 and then boot the PI you should see boot messages start to appear on the PT52 screen!  But wait once you log into your pi you might notice the programs and keys show the wrong output.  You need the set the TERM variable to `TERM=vt52` now programs like nano are useable but you aren't using the PT52 to it fullest just yet. Included is a file called pt52.terminfo you need to compile this on the system your using the terminal command `tic pt52.terminfo`. This will let you use the advanced features available on the PT52 like colour simply change the TERM variable to `TERM=pt52` Now nano looks better.  but this only works on the current user if you try to run nano for example with sudo you get an error pt52 isn't found.  The solution is to run `sudo tic pt52.terminfo` now PT52 is available system wide.  you can also edit your /lib/systemd/system/serial-getty@ service and change **$TERM** to **pt52**.  Now when you login the terminal is setup correctly and doesn't need changing.

## PT52 Escape Sequences

The Escape Sequences are based on the VT52 and in the current state we are almost fulling compatible with the VT52. For more details about the escape sequences look in the [PT52_ESC_CODES.md](PT52_ESC_CODES.md)

## Thanks

I'd like to thank some people from the Raspberry Pi Forums  
Memotech Bill,  
kilograham,  
hippy,  
lars_the_bear,  
EvanEscent,  
scruss,  
Gavinmc42,  
HermannSW  

In some way or another you have shared knowledge, interest, encouragement and for that I Thank You!
