/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2021, DarkElvenAngel
 * All rights reserved.
 * 
 * see LICENCE for full Licence
 */
#include <stdio.h>
#include "pico.h"
#include "pico/stdlib.h"
#include "charbuffer_improved.h"

/**
 * Set background colour
 * 
 * \param colour value 0 - 15
 * \return none
 */
void Set_BColour(uint16_t colour)
{
#ifdef CHARBUFFER_SUPPORT_COLOUR_16 
    if (Cursor.reverse)
        Cursor.foreground_colour = colour % 16;
    else
        Cursor.background_colour = colour % 16;
#endif
#ifdef CHARBUFFER_SUPPORT_COLOUR_256
    if (Cursor.reverse)
        Cursor.foreground_colour = colour % 256;
    else
        Cursor.background_colour = colour % 256;
#endif
}

/**
 * Set foreground colour
 * 
 * \param colour value 0 - 15
 * \return none
 */
void Set_FColour(uint16_t colour)
{
#ifdef CHARBUFFER_SUPPORT_COLOUR_16
    if (Cursor.reverse)
        Cursor.background_colour = colour % 16;
    else
        Cursor.foreground_colour = colour % 16;
#endif
#ifdef CHARBUFFER_SUPPORT_COLOUR_256
    if (Cursor.reverse)
        Cursor.background_colour = colour % 256;
    else
        Cursor.foreground_colour = colour % 256;
#endif
}
/**
 * Set both colours
 * 
 * \param colours
 * \return none
 */
void Set_Colours(uint32_t colours)
{
#ifdef CHARBUFFER_SUPPORT_COLOUR_16
    if (Cursor.reverse)
        Cursor.colours = (colours & 0xF) << 4 | (colours & 0xF0 ) >> 4  ;
    else
        Cursor.colours = colours % 0xFF;
#endif
#ifdef CHARBUFFER_SUPPORT_COLOUR_256
    if (Cursor.reverse)
        Cursor.colours = (colours & 0xFF) << 8 | (colours & 0xFF00 ) >> 4;
    else
        Cursor.colours = colours % 0xFFFF;
#endif
}
/**
 * Set pallet
 * 
 * \param index value 0 - 4
 * \return none
 */
void Set_Pallet(uint8_t index)
{
#if defined(CHARBUFFER_SUPPORT_COLOUR_16) || defined(CHARBUFFER_SUPPORT_COLOUR_256)
    Cursor.Pallet = index % 5;
#endif
}

void Set_Attrib_Underline(bool value)
{
#ifdef FULL_CHARBUFFER
    Cursor.underline = value ;
#endif 
}
/**
 * Set Attribute Reverse
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Reverse(bool value)
{
#ifdef FULL_CHARBUFFER
    if (Cursor.reverse != value)
    {
        uint16_t holder = Cursor.background_colour;
        Cursor.background_colour = Cursor.foreground_colour;
        Cursor.foreground_colour = holder;
    }
    Cursor.reverse = value ;
#endif 
};
/**
 * Set Attribute Bold
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Bold(bool value)
{
#ifdef FULL_CHARBUFFER
    Cursor.bold = value ;
#endif 
}
/**
 * Set Attribute Extended
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Extended(bool value)
{
#ifdef FULL_CHARBUFFER
    Cursor.extended = value ;
#endif
}
/**
 * Set Attribute Flags
 * 
 * \param value Flag
 * \return none
 */
void Set_Attrib_Flags(uint8_t value)
{
#ifdef FULL_CHARBUFFER
    Cursor.Flags = value ;
#endif 
}