#ifndef ATTRIBUTES_H
#define ATTRIBUTES_H
#include "config.h"

#pragma pack(1)
struct Char_Atrrib_s
{
    union
    {
        struct {
            uint8_t underline   : 1;
            uint8_t reverse     : 1;
            uint8_t bold        : 1;
            uint8_t extended    : 1;
        };
        uint8_t Flags;
    };

#ifdef CHARBUFFER_SUPPORT_COLOUR_16
    union
    {
        struct {
            uint8_t foreground_colour   : 4;
            uint8_t background_colour   : 4;
        };
        uint8_t colours;
    };
#endif

#ifdef CHARBUFFER_SUPPORT_COLOUR_256
    union
    {
        struct {
            uint8_t foreground_colour;
            uint8_t background_colour;
        };
        uint16_t colours;
    };
#endif
    uint8_t character;
};// Atrributes;

extern struct Char_Atrrib_s Atrributes;

/**
 * Set background colour
 * 
 * \param colour value 0 - 15
 * \return none
 */
void Set_BColour(uint16_t colour);

/**
 * Set foreground colour
 * 
 * \param colour value 0 - 15
 * \return none
 */
void Set_FColour(uint16_t colour);

/**
 * Set both colours
 * 
 * \param colour value 0 - 15
 * \return none
 */
void Set_Colours(uint32_t colours);

/**
 * Set pallet
 * 
 * \param index value 0 - 4
 * \return none
 */
void Set_Pallet(uint8_t index);

/**
 * Set Atrribute Underline
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Underline(bool value);
/**
 * Set Attribute Reverse
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Reverse(bool value);
/**
 * Set Attribute Bold
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Bold(bool value);
/**
 * Set Attribute Extended
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Extended(bool value);
/**
 * Set Attribute Flags
 * 
 * \param value Flag
 * \return none
 */
void Set_Attrib_Flags(uint8_t value);
#endif