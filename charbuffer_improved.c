/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2021, DarkElvenAngel
 * All rights reserved.
 * 
 * see LICENCE for full Licence
 */
#include "charbuffer_improved.h"
#include "font.h"
#include <stdarg.h>
#include <string.h>

BUFFER_TYPE char_buffer[MAX_LINE][MAX_COL] = { 0 };
#define PRINTF_BUFFER_SIZE 160
char buf_printf[PRINTF_BUFFER_SIZE] = { 0 }; 

/**
 * Memset 12 bytes with 3 byte dataset
 * 
 * \param dest address array
 * \param a MSB of dataset
 * \param b 
 * \param c LSB of dataset
 * \param n size of array to write to 
 * \return none
 */
void memset12(uint32_t *dest, uint8_t a, uint8_t b, uint8_t c, uint32_t n) {
    uint32_t v0 = (a << 24) | (c << 16) | (b << 8) | a;
    uint32_t v2 = (v0 << 8) | c;
    uint32_t v1 = (v2 << 8) | b;
    while (n % 4)
    {
        uint8_t* d = (uint8_t*)dest;
        memset(d++, a, 1);
        memset(d++, b, 1);
        memset(d++, c, 1);
        dest = (uint32_t*)d;
        n--;
    }
    if (n == 0) return;
    {
        n >>= 2; // needs to be multiple of 4
        for(; n>0; n--) {
            *dest++ = v0;
            *dest++ = v1;
            *dest++ = v2;
        }
    } 
}
void memset4(uint32_t *dest, uint32_t data, uint32_t n)
{
    for(; n>0; n--) *dest++ = data;
}

uint8_t Line_Properties[MAX_LINE] = { 0 };
/**
 * Reset Buffer 
 * 
 * \return none
 */
void Reset_Character_Buffer()
{
#ifdef SIMPLE_CHARBUFFER
    memset (char_buffer,0, sizeof(char_buffer));
#elif defined(CHARBUFFER_SUPPORT_COLOUR_16)
    memset12 ((uint32_t*)char_buffer,0, DEFAULT_COLOURS ,0, sizeof(char_buffer) / 3);
#elif defined(CHARBUFFER_SUPPORT_COLOUR_256)  
    memset4 ((uint32_t*)char_buffer, DEFAULT_COLOURS << 8 , sizeof(char_buffer) / 4);
#endif
    memset(&Line_Properties,0, sizeof(uint8_t) * MAX_LINE);
    Buffer_Flags.Line_Wrap = false;
    Set_scrollregion(0,0);
    Home(0);
    Set_Pallet(DEFAULT_PALLET);
    Set_Attrib_Flags(0);
    Set_Colours(DEFAULT_COLOURS);
}

/**
 * Scroll Buffer and create new line.
 * 
 * 0 Down bottom line is truncated, new line added at top
 * 1 Up top line is truncated, newline added at bottom.
 * 
 * \param direction of scroll
 * \return none
 */
void Scroll(bool direction)
{
    #if 1
    if (direction) // Scroll Up
    {
        const uint8_t top = 1;
        const uint8_t bottom = MAX_LINE;
        for (uint8_t i = top; i < bottom; i++)
            memmove(&char_buffer[i-1][0], &char_buffer[i][0], sizeof(char_buffer[0]));// * COLS);
  
#ifdef SIMPLE_CHARBUFFER          
        memset(&char_buffer[MAX_LINE - 1][0], 0, sizeof(char_buffer[0]));
#elif defined(CHARBUFFER_SUPPORT_COLOUR_16)
        memset12 ((uint32_t*)&char_buffer[MAX_LINE - 1][0],0, Cursor.colours ,0, sizeof(char_buffer[0]) / 3);
#elif defined(CHARBUFFER_SUPPORT_COLOUR_256)
        memset4 ((uint32_t*)&char_buffer[MAX_LINE - 1][0], Cursor.colours << 8, sizeof(char_buffer[0]) / 4 );
#endif
    } else {
        for (uint8_t i = MAX_LINE - 1; i > 0; i--)
            memmove((uint32_t*)&char_buffer[i][0], &char_buffer[i-1][0], sizeof(char_buffer[0]));// * COLS);
   
#ifdef SIMPLE_CHARBUFFER          
        memset(&char_buffer[0][0], 0, sizeof(char_buffer[0]));
#elif defined(CHARBUFFER_SUPPORT_COLOUR_16)
        memset12 ((uint32_t*)&char_buffer[0][0],0, Cursor.colours ,0, sizeof(char_buffer[0]) / 3);
#elif defined(CHARBUFFER_SUPPORT_COLOUR_256)
        memset4 ((uint32_t*)&char_buffer[0][0],Cursor.colours << 8, sizeof(char_buffer[0]) / 4);
#endif        
    }
    #endif
}

/**
 * Place cursor at a specific location
 * 
 * \param column Column number
 * \param line Line number
 * \return none
 */
void Move_cursorto(uint8_t column, uint8_t line)
{
    if (column < MAX_COL ) Cursor.Column = column;
    if (line   < MAX_LINE) Cursor.Line = line;
}
/**
 * Move the cursor in on direction
 * 
 * \param plane 0 up/down : 1 left/right 
 * \param n number or positions
 * \return none
 */
void Move_cursor(bool plane, int16_t n)
{
    if (plane)
    {
        if (Cursor.Column + n >= MAX_COL) { Cursor.Column = MAX_COL - 1; return; }
        if (Cursor.Column + n < 0 ){ Cursor.Column = 0; return; }
        Cursor.Column += n;
    } else {
        if (Cursor.Line + n >= MAX_LINE) { Cursor.Line = MAX_LINE - 1; return; }
        if (Cursor.Line + n < 0 ){ Cursor.Line = 0; return; }
        Cursor.Line += n;
    }
}
/**
 * Save cursor location and if enabled attributes and colour
 * 
 * \return none
 */
void Save_cursor()
{
    memcpy (&Cursor_Store, &Cursor, sizeof(struct CPostion_s));
}
/**
 * Restore cursor location and if enabled attributes and colour
 * 
 * \return none
 */
void Restore_cursor()
{
    memcpy (&Cursor, &Cursor_Store, sizeof(struct CPostion_s));
}

/**
 * Place the cursor at the home position
 * 
 * \param mode 0 Origin : 1 Line
 * \return none
 */
void Home(bool mode)
{
    Cursor.Column = 0;
    if (!mode) Cursor.Line = 0;
}
/**
 * Clear Screen
 * 
 * 0 from cursor to end of buffer
 * 1 from begining of buffer to cursor
 * 2 entire buffer
 * 
 * \param option select operation
 * \return none
 */
void Clear_screen(uint8_t option)
{
    switch (option)
    {
        case 0: // Erase to end of screen from cursor
#ifdef SIMPLE_CHARBUFFER
        memset(&char_buffer[Cursor.Line][Cursor.Column],
            0,
            (&char_buffer[MAX_LINE-1][MAX_COL] - &char_buffer[Cursor.Line][Cursor.Column]) * sizeof(BUFFER_TYPE)  // &char_buffer[ST_Settings.Margin_Bottom - 1][MAX_COL] - 
        );
#elif defined(CHARBUFFER_SUPPORT_COLOUR_16)
        memset12 ((uint32_t*)&char_buffer[Cursor.Line][Cursor.Column],0, Cursor.colours ,0, &char_buffer[MAX_LINE-1][MAX_COL] - &char_buffer[Cursor.Line][Cursor.Column]);
#elif defined(CHARBUFFER_SUPPORT_COLOUR_256)
        memset4 ((uint32_t*)&char_buffer[Cursor.Line][Cursor.Column], Cursor.colours << 8, &char_buffer[MAX_LINE-1][MAX_COL] - &char_buffer[Cursor.Line][Cursor.Column]);
#endif 
        for (int i = Cursor.Line; i < MAX_LINE; i++) Line_Properties[i] = 0;
        break;
        case 1: // Erase to beginning of screen from cursor 
#ifdef SIMPLE_CHARBUFFER
        memset(&char_buffer[0][0],
            0,
            (&char_buffer[Cursor.Line][Cursor.Column] - &char_buffer[0][0]) * sizeof(BUFFER_TYPE)
            //sizeof(struct st_char) * MAX_COL * (MAX_LINE - 2)
        );
#elif defined(CHARBUFFER_SUPPORT_COLOUR_16)
        memset12 ((uint32_t*)&char_buffer[0][0],0, Cursor.colours ,0, (&char_buffer[Cursor.Line][Cursor.Column] - &char_buffer[0][0]));
#elif defined(CHARBUFFER_SUPPORT_COLOUR_256)
        memset4 ((uint32_t*)&char_buffer[0][0], Cursor.colours << 8, &char_buffer[Cursor.Line][Cursor.Column] - &char_buffer[0][0]);
#endif 
        for (int i = 0; i < Cursor.Line; i++) Line_Properties[i] = 0;
        break;
        case 2:    
#ifdef SIMPLE_CHARBUFFER
        memset(&char_buffer[0][0],0, sizeof(BUFFER_TYPE) * MAX_COL * MAX_LINE);
#elif defined(CHARBUFFER_SUPPORT_COLOUR_16)
        memset12 ((uint32_t*)&char_buffer[0][0],0, Cursor.colours ,0, (sizeof(BUFFER_TYPE) * MAX_COL * MAX_LINE) / 3);
#elif defined(CHARBUFFER_SUPPORT_COLOUR_256)
        memset4 ((uint32_t*)&char_buffer[0][0], (uint32_t)Cursor.colours << 8, (sizeof(BUFFER_TYPE) * MAX_COL * MAX_LINE) / 4);
#endif 

        memset(&Line_Properties,0, sizeof(uint8_t) * MAX_LINE);
        break;
    }
}
/**
 * Clear Current Line
 * 
 * 0 from cursor to end of line
 * 1 from begining of line to cursor
 * 2 entire line
 * 
 * \param option select operation
 * \return none
 */
void Clear_line(uint8_t option)
{
    switch (option)
    {
        case 0: // Erase to end of line from cursor
#ifdef SIMPLE_CHARBUFFER
        memset(&char_buffer[Cursor.Line][Cursor.Column],
            0,
            (&char_buffer[Cursor.Line][MAX_COL] - &char_buffer[Cursor.Line][Cursor.Column]) * sizeof(BUFFER_TYPE)
        );
#else
#ifdef CHARBUFFER_SUPPORT_COLOUR_16
        memset12 ((uint32_t*)&char_buffer[Cursor.Line][Cursor.Column],0, Cursor.colours ,0, MAX_COL - Cursor.Column);
#elif defined(CHARBUFFER_SUPPORT_COLOUR_256)
        memset4((uint32_t*)&char_buffer[Cursor.Line][Cursor.Column], Cursor.colours << 8, MAX_COL - Cursor.Column);
#endif
#endif
        break;
#ifdef SIMPLE_CHARBUFFER
        case 1:  // Erase to beginning of line from cursor
        memset(&char_buffer[Cursor.Line][0],
            0,
            (&char_buffer[Cursor.Line][Cursor.Column] - &char_buffer[Cursor.Line][0]) * sizeof(BUFFER_TYPE)
        );
#else
#ifdef CHARBUFFER_SUPPORT_COLOUR_16
        memset12 ((uint32_t*)&char_buffer[Cursor.Line][0],0, Cursor.colours ,0, Cursor.Column);
#elif defined(CHARBUFFER_SUPPORT_COLOUR_256)
        memset4((uint32_t*)&char_buffer[Cursor.Line][0], Cursor.colours << 8, Cursor.Column);
#endif
#endif
        break;
        case 2: // Erase line 
#ifdef SIMPLE_CHARBUFFER
        memset(&char_buffer[Cursor.Line][0],
            0,
            (&char_buffer[Cursor.Line][MAX_COL] - &char_buffer[Cursor.Line][0]) * sizeof(BUFFER_TYPE)
        );
#else
#ifdef CHARBUFFER_SUPPORT_COLOUR_16
        memset12 ((uint32_t*)&char_buffer[Cursor.Line][0],0, Cursor.colours ,0, sizeof(char_buffer[0]) / 3 );
#elif defined(CHARBUFFER_SUPPORT_COLOUR_256)
        memset4((uint32_t*)&char_buffer[Cursor.Line][0], Cursor.colours << 8, (&char_buffer[Cursor.Line][MAX_COL] - &char_buffer[Cursor.Line][0]));
#endif
#endif 

        break; 
    }
}
/**
 * Set scrolling region
 * This is use to only scroll part of the display
 * 
 * \param top line number where region starts.  use 0 to reset
 * \param bottom line number where region ends. use 0 for reset 
 * \return none
 */
void Set_scrollregion(uint8_t top, uint8_t bottom)
{
    if (top == bottom == 0)                             // RESET TO DEFAULT
    {
        Buffer_Flags.Top_Margin = 0;
        Buffer_Flags.Bottom_Margin = MAX_LINE - 1;
        return;
    }
    if (top > bottom || bottom > MAX_LINE - 1) return;  // ILLEGAL
    if ((bottom - top) > 1)                             // Valid
    {
        Buffer_Flags.Top_Margin = top;
        Buffer_Flags.Bottom_Margin = bottom;
    }
    return;
}

/// writing to the buffer

/**
 * put a character at a location.
 * This function does not effect cursor location.
 * 
 * \param column Column number
 * \param line Line number
 * \param character what to place
 * \return none
 */
void Put_charp(uint8_t column, uint8_t line, const char character);
/**
 * put a character at the current position.
 * This function does effect cursor location!
 * 
 * \param character what to place
 * \return none
 */
void Put_char(const char character)
{
#ifdef SIMPLE_CHARBUFFER
    char_buffer[Cursor.Line][Cursor.Column++] = character;
#else
#if defined (CHARBUFFER_SUPPORT_COLOUR_16) || defined (CHARBUFFER_SUPPORT_COLOUR_256)
    char_buffer[Cursor.Line][Cursor.Column].colours = Cursor.colours;
#endif
    char_buffer[Cursor.Line][Cursor.Column].Flags = Cursor.Flags;
    if (Cursor.extended && (character >= FONTSET_START && character <= FONTSET_START + FONTSET_SIZE)) 
        char_buffer[Cursor.Line][Cursor.Column].character = character - FONTSET_START;
    else
    {
        char_buffer[Cursor.Line][Cursor.Column].extended = false;
        char_buffer[Cursor.Line][Cursor.Column].character = character;
    }
    Cursor.Column++;
#endif
}
/**
 * Write formated output to the buffer and update position of cursor
 * 
 * \param format controls the output as in C printf.
 * \return none
 */
int Printf (const char *format, ...)
{
    va_list args;
    va_start(args, format);
    int r = vsprintf(buf_printf, format, args);
    va_end(args);

    for (char *Ch = buf_printf; *Ch != 0; *Ch++) Decode_Character(*Ch);

    memset(buf_printf,0,PRINTF_BUFFER_SIZE);
    return r;
}
/**
 * Write a string, followed by a newline, to screen.
 * 
 * \param const char 
 * \return none
 */
void Puts(const char *s)
{
    for (; *s != 0; *s++) Decode_Character(*s);
    Enter();
}

/**
 * Write formatted output to buffer at a location.
 * This function does not effect cursor location, and doesn't decode control characters.
 * 
 * \param column Column number
 * \param line Line number
 * \param Format controls the output as in C printf.
 * \return none
 */
void Printfp (uint8_t column, uint8_t line, const char *format, ... )
{
    va_list args;
    va_start(args, format);
    vsprintf(buf_printf, format, args);
    va_end(args);

    for (char *Ch = buf_printf; *Ch != 0; *Ch++)
    {
        if (column >= MAX_COL - 1)
        {
             column = MAX_COL - 1;
        }
        Put_charp(column++, line, *Ch >= 32 ? *Ch : 0);
    }

    memset(buf_printf,0,PRINTF_BUFFER_SIZE);
}

//
/**
 * CarriageReturn
 * 
 * \return none
 */
void CarriageReturn() { Cursor.Column = 0; }
/**
 * Linefeed
 *  
 * \return none
 */
void Linefeed() 
{
    Cursor.Line++;
    Cursor_Position_Check();
    // Move_cursor(0, 1);
}
/**
 * Reverse_Linefeed
 *  
 * \return none
 */
void Reverse_Linefeed()
{
    Cursor.Line--;
    // Move_cursor(0, -1);
}
/**
 * Backspace
 * 
 * \return none
 */
void Backspace() { if (Cursor.Column) Cursor.Column--; };
/**
 * Tab
 * 
 * \return none
 */
void Tab()
{
    for (uint8_t Ts = 7; Ts < MAX_COL; Ts += 8)
        if (Cursor.Column >= Ts) 
        {
            continue;
        }
        else
        {
            Cursor.Column = Ts;
            return;
        }
}
/**
 * Enter
 * 
 * \return none
 */
void Enter()
{
    CarriageReturn();
    Linefeed();
}

/**
 * Cursor Position Check verify Cursor is within buffer bounds.
 * 
 * \return none
 */
void Cursor_Position_Check()
{
    static bool Home_Flag = true;
    if(Cursor.Line == 0) Home_Flag = true;
    if (Cursor.Column >= MAX_COL) 
    {
        if (Buffer_Flags.Line_Wrap)
        {
            Cursor.Column = 0;
            Cursor.Line++;
        } else {
            Cursor.Column = MAX_COL - 1;
        }
    }
    if (Cursor.Line >= MAX_LINE)
    {
        Scroll(1);
        Cursor.Line = MAX_LINE - 1;
    }
    if (Cursor.Line < 0)
    {
        Scroll(0);
        Cursor.Line = 0;
    }

    /*
    if(ST_Settings.Margin_Bottom != 0)
    { if( Cursor.Line > ST_Settings.Margin_Bottom - 1) scroll(); }
    else
    { if( Cursor.Line >= FOOTER) scroll();  }
    if( !Home_Flag && Cursor.Line < ST_Settings.Margin_Top) scroll_back();
    if(Cursor.Line > ST_Settings.Margin_Top) Home_Flag = false;
    */
}


extern bool _Prase_ESC(char data);

/**
 * Decode Character this is normally called from within Printf and not by the main program.
 * 
 * \param character what character to decode.
 * \return none
 */
void Decode_Character(char character)
{
    static bool In_Esc_Sequence = false;
    if (In_Esc_Sequence) 
    {
        In_Esc_Sequence = _Prase_ESC(character);
    } else {
        if (character < 0x20 || character == 0x7F)    // DEAL WITH CONTROL Characters
        {
        
            switch (character)
            {
                case 0x08: Backspace(); break;      // Backspace
                case 0x09: Tab(); break;            // Tab
                case 0x0A:                          // Line feed
                    Linefeed();
                    if (Buffer_Flags.Cr_Lf)
                        CarriageReturn();//Linefeed();
                    break;
                case 0x0D:                          // Carriage return
                    CarriageReturn();
                    //if (Buffer_Flags.Cr_Lf)
                    //    Linefeed();
                    break;  
                case 0x0E:                          // Switch to normal font mode
                    Set_Attrib_Extended(false);
                    break;                          // Shift out
                case 0x0F:                          // Switch to extened font mode.
                    Set_Attrib_Extended(true);
                    break;                          // Shift in
                case 0x1B:                          // ESCAPE 
                    In_Esc_Sequence = true;
                    break;
                default: break;                     // All other's do nothing
            }
            //char_buffer[Cursor.Line][Cursor.Column++] = (character + 0x100);
        } else {                                    // This is a printable character
            Put_char(character);
        }
    }

    Cursor_Position_Check();
}
