#ifndef CHARBUFFER_IMPROVED_H
#define CHARBUFFER_IMPROVED_H

#include <stdio.h>
#include "pico.h"
#include "pico/stdlib.h"
#include "attributes.h"
#include <stdarg.h>
#include "config.h"

// OVERRIDE STDOUT FUNCTIONS WITH INTERNAL VERSIONS
#undef printf
#define printf Printf
#undef putchar
#define putchar Decode_Character
#undef puts
#define puts Puts

#define MAX_COL CHARBUFFER_COLUMNS
#define MAX_LINE CHARBUFFER_LINES
#define DEFAULT_COLOURS CHARBUFFER_DEFAULT_COLOURS
#define DEFAULT_PALLET CHARBUFFER_DEFAULT_PALLET
#pragma pack(1)

struct CPostion_s
{
    int8_t Column;
    int8_t Line;
#ifdef FULL_CHARBUFFER
    union
    {
        struct {
            uint8_t underline   : 1;
            uint8_t reverse     : 1;
            uint8_t bold        : 1;
            uint8_t extended    : 1;
        };
        uint8_t Flags;
    };
#ifdef CHARBUFFER_SUPPORT_COLOUR_16
    union
    {
        struct {
            uint8_t foreground_colour   : 4;
            uint8_t background_colour   : 4;
        };
        uint8_t colours;
    };
    int8_t Pallet;
#endif
#ifdef CHARBUFFER_SUPPORT_COLOUR_256
    union
    {
        struct {
            uint8_t foreground_colour;
            uint8_t background_colour;
        };
        uint16_t colours;
    };
    int8_t Pallet;
#endif
#endif
} Cursor, Cursor_Store;
struct CB_Flags_s
{
    uint8_t Line_Wrap           : 1;
    uint8_t Cr_Lf               : 1;
    uint8_t Top_Margin;
    uint8_t Bottom_Margin;

} Buffer_Flags;

extern BUFFER_TYPE char_buffer[MAX_LINE][MAX_COL];
/**
 * Reset Buffer 
 * 
 * \return none
 */
void Reset_Character_Buffer();

/**
 * Scroll Buffer and create new line.
 * 
 * 0 Down bottom line is truncated, new line added at top
 * 1 Up top line is truncated, newline added at bottom.
 * 
 * \param direction of scroll
 * \return none
 */
void Scroll(bool direction);
/**
 * Place cursor at a specific location
 * 
 * \param column Column number
 * \param line Line number
 * \return none
 */
void Move_cursorto(uint8_t column, uint8_t line);
/**
 * Move the cursor in on direction
 * 
 * \param plane 0 up/down : 1 left/right 
 * \param n number or positions
 * \return none
 */
void Move_cursor(bool plane, int16_t n);
/**
 * Place the cursor at the home position
 * 
 * \param mode 0 Origin : 1 Line
 * \return none
 */
/**
 * Save cursor location and if enabled attributes and colour
 * 
 * \return none
 */
void Save_cursor();
/**
 * Restore cursor location and if enabled attributes and colour
 * 
 * \return none
 */
void Restore_cursor();

void Home(bool mode);
/**
 * Clear Screen
 * 
 * 0 from cursor to end of buffer
 * 1 from begining of buffer to cursor
 * 2 entire buffer
 * 
 * \param option select operation
 * \return none
 */
void Clear_screen(uint8_t option);
/**
 * Clear Current Line
 * 
 * 0 from cursor to end of line
 * 1 from begining of line to cursor
 * 2 entire line
 * 
 * \param option select operation
 * \return none
 */
void Clear_line(uint8_t option);
/**
 * Set scrolling region
 * This is use to only scroll part of the display
 * 
 * \param top line number where region starts.  use 0 to reset
 * \param bottom line number where region ends. use 0 for reset 
 * \return none
 */
void Set_scrollregion(uint8_t top, uint8_t bottom);

/// writing to the buffer

/**
 * put a character at a location.
 * This function does not effect cursor location.
 * 
 * \param column Column number
 * \param line Line number
 * \param character what to place
 * \return none
 */
void Put_charp(uint8_t column, uint8_t line, const char character);
/**
 * put a character at the current position.
 * This function does effect cursor location!
 * 
 * \param character what to place
 * \return none
 */
void Put_char(const char character);
/**
 * Write formated output to the buffer and update position of cursor
 * 
 * \param character what to place
 * \return number  of characters printed
 */
int Printf (const char *format, ...);
/**
 * Write a string, followed by a newline, to screen.
 * 
 * \param const char 
 * \return none
 */
void Puts(const char *s);
/**
 * Write formatted output to buffer at a location.
 * This function does not effect cursor location.
 * 
 * \param column Column number
 * \param line Line number
 * \param Format controls the output as in C printf.
 * \return none
 */
void Printfp (uint8_t column, uint8_t line, const char *format, ... );

//
/**
 * CarriageReturn
 * 
 * \return none
 */
void CarriageReturn();
/**
 * Linefeed
 *  
 * \return none
 */
void Linefeed(); 
/* Reverse_Linefeed
 *  
 * \return none
 */
void Reverse_Linefeed();
/**
 * Backspace
 * 
 * \return none
 */
void Backspace();
/**
 * Tab
 * 
 * \return none
 */
void Tab();
/**
 * Enter
 * 
 * \return none
 */
void Enter();
/**
 * Cursor Position Check verify Cursor is within buffer bounds.
 * 
 * \return none
 */
void Cursor_Position_Check();
/**
 * Decode_Character
 * 
 * \param character what character to decode.
 * \return none
 */
void Decode_Character(char character);

#endif