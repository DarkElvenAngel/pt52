/**
 * Configure the compile time setting
 * 
 *  Added support for 16 colour mode.
 *  Added Baud Rate setting
 *  Added VGA scaling options
 */
#ifndef CHARBUFFER_COLOURS
// colour modes are 16 and 256 all other values select Monochrome
#define CHARBUFFER_COLOURS 256
// How many line to Display
#define CHARBUFFER_LINES 24
// How many columns
#define CHARBUFFER_COLUMNS 80
// This Setting does nothing in Monochrome mode
#define CHARBUFFER_DEFAULT_COLOURS 0x0007
#define CHARBUFFER_DEFAULT_PALLET 0
// SERIAL SETTINGS [Static for now] 
#define BAUD_RATE 9600//115200
#define DATA_BITS 8
#define STOP_BITS 1
#define PARITY    UART_PARITY_NONE
// VGA Settings
#define VGA_X_SCALING 1
#define VGA_Y_SCALING 2
// This feature isn't enabled it made my monitor very angry
#define VGA_SCANLINES 0

// ============================================================================
// /!\ DO NOT EDIT BELOW THIS LINE IT WILL NOT GO WELL IF YOU MAKE A MISTAKE!!!
// ============================================================================
#if CHARBUFFER_COLOURS == 16
#define CHARBUFFER_SUPPORT_COLOUR_16 
#endif
#if CHARBUFFER_COLOURS == 256
// #error "Can not use this colour mode isn't available"
#define CHARBUFFER_SUPPORT_COLOUR_256 
#endif
#if !defined(CHARBUFFER_SUPPORT_COLOUR_16) && !defined(CHARBUFFER_SUPPORT_COLOUR_256)
#define SIMPLE_CHARBUFFER 
#define BUFFER_TYPE uint8_t
#else
#define FULL_CHARBUFFER
#define BUFFER_TYPE struct Char_Atrrib_s
#endif
#define VGA_MAXLINES 60
#if CHARBUFFER_LINES > VGA_MAXLINES / VGA_Y_SCALING
#warning "config.h - CHARBUFFER_LINES is too large for the selected scaling. Adjusting value to fix scale"
#undef CHARBUFFER_LINES
#define CHARBUFFER_LINES VGA_MAXLINES / VGA_Y_SCALING
#endif
// If you have a less lines that is possible on the display this setting overs the test down # of lines
#define VGA_FIRST_LINE ((VGA_MAXLINES / VGA_Y_SCALING) - CHARBUFFER_LINES ) / 2
#define VGA_MAXCOLUMNS 80
#if CHARBUFFER_COLUMNS > VGA_MAXCOLUMNS / VGA_X_SCALING
#warning "config.h - CHARBUFFER_COLUMNS is too large for the selected scaling. Adjusting value to fix scale"
#undef CHARBUFFER_COLUMNS
#define CHARBUFFER_COLUMNS VGA_MAXCOLUMNS / VGA_X_SCALING
#endif
#endif