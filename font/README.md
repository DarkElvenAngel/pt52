# Fonts

This is where the fonts are stored for the PT52

## Font files

font.bold.c         - This is the bold version of the characters range 32 - 126
font.reg.c          - This is the thin version of the characters range 32 - 126
font.petscii.c      - This contains PETSCII only is the range 128 +
font.petscii_thin.c - This contains PETSCII thin version only is the range 128 +

NOTE: The PETSCII font files contain no other font data.

### Purpose

For simplicity only the bold and regular fonts are included at compile time.  The font mapping is currently incomplete the goal to include extended characters must be done in a way the lends to most common characters being available in the correct locations.  examples would include pi or the degrees symbols.

### Limits

The Terminal software doesn't print any character lower than 32 or space these are left blank however the will be a control character set available that will decode control characters like ES for Escape.

### Full font

There is a full ASCII code page 437 font that can be substituted for the current set.  I haven't included here yet as the font system isn't yet compatible with it.

## Font Tool

Convert Character ROM dump to C

### How to use it

You need to create a .layout file for your font this tell the converter what character goes where.  The converter is made for 8 x 8 fonts only like the ones found in the C64

### Layout file format

The layout file is a binary file 256 bytes in size, each byte tells the converter what character is in that location in the bin file.  example the C64 first character in the bin is '@' so the layout will have an @ as it's first byte.
