#include <stdio.h>
#include <string.h>

unsigned char Buffer[8];
// #define IFILE "901447-10m.bin"
// #define LFILE "901447-10m.layout"
// #define OFILE "font_901447.c"

unsigned char Font_Buffer[0x100*8] = { 0 };
unsigned char Output_Buffer[0x100*8] = { 0 };
int BlankChk_buffer[0x100] = { 0 };
int Map_buffer[0x100] = { 0 };

int layout_ptr = 0;

int main(int argc, char *argv[])
{
    FILE *Input_File;
    FILE *Layout_File;
    FILE *Output_File;
    if (argc < 4 || argc > 5) { printf ("USAGE :  %s  BIN_FILE LAYOUT_FILE OUTPUT_FINE", argv[0]); return 1; }

    unsigned short Count = 0;

    Input_File = fopen(argv[1], "rb");
    Layout_File = fopen(argv[2],"rb");
    Output_File = fopen(argv[3], "w");

    fprintf(Output_File,"// AUTO DUMP OFF %s\n#include \"font8x8.h\"\n\nbyte fontdata[0x100 * 8] = {\n",argv[1]);

    while (fread(Buffer,sizeof(Buffer), 1, Input_File) != 0)
    {
        memcpy(&Font_Buffer[Count * 8], &Buffer, sizeof(Buffer));
        char pos =(char)fgetc(Layout_File);
        if (pos != ' ') memcpy(&Output_Buffer[pos * 8], &Buffer, sizeof(Buffer));
        Map_buffer[Count++] = pos;
    }
    //for (int j = 0; j < 0x100; j++)
    //    if (memcmp(Font_Buffer + (j * 8), Output_Buffer + (k * 8), 8) == 0) 
    //    {
    //        BlankChk_buffer[j] = 1;
    //    }
    Count = 0;
    for (unsigned char *b = Output_Buffer; Count < 0x100; b+=8)
    {
        // printf ("Read Char %03d ", Count++);
         fprintf(Output_File,"\t0x%02X, 0x%02X, 0x%02X, 0x%02X, 0x%02X, 0x%02X, 0x%02X, 0x%02X,\t// Index \'%c\' 0x%02X \n",
            (char)*b,
            (char)*(b + 1),
            (char)*(b + 2),
            (char)*(b + 3),
            (char)*(b + 4),
            (char)*(b + 5),
            (char)*(b + 6),
            (char)*(b + 7),
            Count < ' ' ? ' ' : Count ,
            Count,
            BlankChk_buffer[Count++] ? "BLANK CHARACTER" : ""
        ); /**/
    }
    fprintf(Output_File,"};\n");
    fclose(Input_File);
    fclose(Layout_File);
    fclose(Output_File);
    return 0;
}