/** 
 *  Iceberg 
 * 
 * Your hull is badly damaged and
 * you've no weapons to speak of. As
 * you limp slowly home through
 * treacherous iceberg-strew waters,
 * you become aware that an enemy
 * ship is tailing you.  Strangely
 * it can detect you but not the
 * icebergs, your best chance is to
 * lure them in hitting one.
 */
#include <stdio.h>
#include <stdlib.h> // system()
#include <string.h> // memset()
#include <ctype.h>  // tolower()
#include "bsp/board.h" // board_millis()
#include "pt52io.h"

#define WATER_CHAR      'w'
#define P_BOAT_CHAR     'a'
#define E_BOAT_CHAR     'b'
#define ICEBERG_CHAR    'c'


int app_iceberg(int argc, char** filename)
{
    const byte ship[8] =
    {
        0x00, 0x24, 0x24, 0x36, 0x36, 0x24, 0xff, 0x7e
    };
    const byte iceberg[8] =
    {
        0x00, 0x08, 0x18, 0x3c, 0x3c, 0x7e, 0x33, 0xcc
    };
    const byte water[8] =
    {
        0x00, 0x00, 0x33, 0xcc, 0x00, 0x00, 0x33, 0xcc
    };
    Reset_Character_Buffer();                   // Start with a clear screen
    printf("\eb\044\eS\ef\057 ICEBERG\es\ef\047%80s\eb\040\r\n\n"," ");
    Define_User_Char(P_BOAT_CHAR,ship);
    Define_User_Char(E_BOAT_CHAR,ship);
    Define_User_Char(ICEBERG_CHAR,iceberg);
    Define_User_Char(WATER_CHAR,water);
    srand(board_millis());
    Set_FColour(7);
    Set_BColour(0);
    Enter();
    Save_cursor();
    puts ("Your hull is badly damaged and you've no weapons to speak of.\n\r\
As you limp slowly home through treacherous iceberg-strew waters,\n\r\
you become aware that an enemy ship is tailing you. Strangely it\n\r\
can detect you but not the icebergs, your best chance is to\n\r\
lure them in hitting one.\n\n\n\rPRESS ANY KEY TO BEGIN");
    getchar();
    bool playagain = true;
    do
    {
    char B[8][8] = { 0 };
    uint8_t N = (rand() % 8) + 4;

    for (uint8_t I = 1; I < N; I++)
        B[rand() % 8][rand() % 8] = ICEBERG_CHAR;
    
    uint8_t SX, SY;
    do
    {
        SX = rand() % 8;
        SY = rand() % 8;
    } while (B[SX][SY] != 0);
    B[SX][SY] = E_BOAT_CHAR;
    
    uint8_t YX, YY;
    do
    {
        YX = rand() % 8;
        YY = rand() % 8;
    } while (B[YX][YY] != 0);
    B[YX][YY] = P_BOAT_CHAR;

    while(1)
    {
        Restore_cursor();
        Clear_screen(0);
        for (uint8_t Y = 0; Y < 8; Y++)
        {
            Tab();
            for (uint8_t X = 0; X < 8; X++)
            {
                Set_FColour(B[X][Y] ? 15 : 4);
                if (B[X][Y] == E_BOAT_CHAR) Set_BColour(1);
                else if (B[X][Y] == P_BOAT_CHAR) Set_BColour(12);
                else Set_BColour(0);
                printf ("\017%c\016", B[X][Y] ?  B[X][Y] : WATER_CHAR);
            }
            Enter();
        }
        Set_BColour(0);
        Set_FColour(7);
        Enter();
        B[YX][YY] = 0;
        puts("Direction ( N, E, S, W ) ");
        switch (toupper(getchar()))
        {
            case 'S': YY += (YY != 7); break;
            case 'N': YY -= (YY != 0); break;
            case 'E': YX += (YX != 7); break;
            case 'W': YX -= (YX != 0); break;
        }
        if (B[YX][YY] != 0) break;
        B[YX][YY] = P_BOAT_CHAR;
        B[SX][SY] = 0;

        SX += ((YX-SX) > 0) ? 1 : (((YX-SX) < 0) ? -1 : 0);
        SY += ((YY-SY) > 0) ? 1 : (((YY-SY) < 0) ? -1 : 0);

        if (B[SX][SY] != 0) break;
        B[SX][SY] = E_BOAT_CHAR;
    }

    if (B[YX][YY] == E_BOAT_CHAR)
    {
        puts("You've been caught!");
        // return 0;
    }
    else if (B[SX][SY] == P_BOAT_CHAR)
    {
        puts("You've been caught!");
        // return 0;
    }
    else if (B[YX][YY] == ICEBERG_CHAR)
    {
        puts("You've hit an iceberg");
        // return 0;
    }
    else if (B[SX][SY] == ICEBERG_CHAR)
    {
        puts("You're safe - he's hit one");
        // return 0;
    }

    puts("Play again?");
    playagain = (tolower(getchar()) == 'y' ? true : false);
    } while (playagain);
    puts("\eH\eJ");
}