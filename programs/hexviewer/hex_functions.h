/*
The MIT License (MIT)

Copyright (c) 2014 Ivaylo Stoyanov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/// This source code has been modified to expand capabilities and run on the PT52
#ifndef HEX_FUNC_H
#define HEX_FUNC_H
#include "pt52io.h"

#define FILE_MODE 0

unsigned getPageCount(uint32_t address);
unsigned getFilePageCount(FILE *pSrc);
void printHex(uint32_t address,unsigned long page);
void printFileHex(FILE *pSrc, unsigned long page);
void hex_menu(uint32_t* basemem);

#endif