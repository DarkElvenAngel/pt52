/*
The MIT License (MIT)

Copyright (c) 2014 Ivaylo Stoyanov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/// This source code has been modified to expand capabilities and run on the PT52

#include <stdio.h>
#include <stdlib.h> 
#include <string.h> 
#include <ctype.h> 
#include "../../pt52io.h"

#include "hex_functions.h"


int app_hexview(int argc, char** filename)
{
  unsigned long currentPage = 0, pageCount = 0;
  bool file_mode = argc >= 2 ? 1 : 0;
  uint32_t base_address = 0;
  // char fileName[FILENAME_MAX];
//#if FILE_MODE == 1
  FILE *pFile;
  if (file_mode) {

    // puts("HEX VIEWER");

    if (filename[1] == NULL) {
      puts("Usage: hex <file name>");
      return 1;
    }

    if ((pFile = fopen(filename[1], "rb")) == NULL)
    {
      puts("Cannot open file");
      return 1;
    }

    pageCount = getFilePageCount(pFile);
  } else {

  pageCount = getPageCount(base_address);
}
  for (;;) {
    // Clear screen
    Clear_screen(2);
    Home(0);

    if (file_mode) {
      printf("\eR  HEX VIEWER        [ %s ]                                                          \er", filename);
      Move_cursorto(0,2);
      printFileHex(pFile, currentPage);
    } else {
      puts("\eR  HEX VIEWER        [ BOOT ROM ]                                                \er");
      Move_cursorto(0,2);
      printHex(base_address, currentPage);
    }
    //printf("File: %s\n", "argv[1]");
    // puts("Internal Memory");

    Move_cursorto(0,22);
    puts("______________________________________________________________________________________");
    printf("\eR Page [ %3lu/%3lu ] (M)enu (P)revious (N)ext (Q)uit?                                  \er", currentPage + 1, pageCount);
    Home(1);
    // Parse keyboard input
    switch (tolower(getchar())) {
        case 'm':
          if (!file_mode){
            hex_menu(&base_address);
            pageCount = getPageCount(base_address);
          }
            currentPage = 0;
            break;
      case 'p':
        if (currentPage > 0) --currentPage;
        break;
      case 'n':
        if (currentPage+1 < pageCount) ++currentPage;
        break;
      case 'q':
        if (file_mode) fclose(pFile);
        printf ("\eH\eJ");
        return 0;
    }
  }
  return 0;
}