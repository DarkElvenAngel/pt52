// PT52 Shell


#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ptsh.h"
//#include "pt52dc.h"
#include "pt52io.h"

jmp_buf ptsh_buffer;


void PTSH_EXIT(int e)
{
  printf ("EXIT code [%d]\n\r",e);
  longjmp(ptsh_buffer, 1);
}
#define MK_CMD(x) int ptsh_ ## x (int argc,char **args)
#define MK_APP(x) int app_ ## x (int argc,char **args)
/*
  Function Declarations for builtin shell commands:
 */
MK_CMD(cd);
MK_CMD(mem);
MK_APP(hexview);
MK_APP(term);
MK_APP(intepret);
MK_CMD(cls);
MK_APP(iceberg);
//MK_CMD(basic);
MK_CMD(test);
MK_CMD(exit);
MK_APP(kilo);
MK_CMD(ls);
MK_CMD(type);
MK_CMD(help);
MK_CMD(dummy);
MK_CMD(ver);
MK_CMD(mkdir);
MK_CMD(move);
MK_CMD(copy);
MK_CMD(del);
MK_CMD(echo);

/*
  List of builtin commands, followed by their corresponding functions.
 */
#define xstr(s) #s
#define N_CMD(x) xstr(x) 
char *builtin_str[] = {
  N_CMD(cd),
  N_CMD(mem),
  N_CMD(hex),
  N_CMD(term),
  N_CMD(run),
  N_CMD(cls),
  N_CMD(iceberg),
 // N_CMD(basic),
  N_CMD(test),
  N_CMD(exit),
  N_CMD(edit), // Alias Kilo to edit
  N_CMD(ls),
  N_CMD(type),
  N_CMD(help),
  N_CMD(dummy), 
  N_CMD(ver),
  N_CMD(mkdir),
  N_CMD(move),
  N_CMD(copy),
  N_CMD(del),
  N_CMD(echo),
};
#undef xstr
#define F_CMD(x) &ptsh_ ## x 
#define F_APP(x) &app_## x
int (*builtin_func[]) (int,char **) = {
  F_CMD(cd),
  F_CMD(mem),
  F_APP(hexview),
  F_APP(term),
  F_APP(intepret),
  F_CMD(cls),
  F_APP(iceberg),
//  F_CMD(basic),
  F_CMD(test),
  F_CMD(exit),
  F_APP(kilo),
  F_CMD(ls),
  F_CMD(type),
  F_CMD(help),
  F_CMD(dummy),
  F_CMD(ver),
  F_CMD(mkdir),
  F_CMD(move),
  F_CMD(copy),
  F_CMD(del),
  F_CMD(echo),
};

int ptsh_num_builtins() {
  return sizeof(builtin_str) / sizeof(char *);
}

int ptsh_help(int argc,char **args)
{
  int i;
  puts("PT52 SHELL");
  printf("version %s\n\r", PTSH_VERSION);
  puts("Type program names and arguments, and hit enter.");
  puts("The following are built in:");

  for (i = 0; i < ptsh_num_builtins(); i++) {
    printf("  %s\n\r", builtin_str[i]);
  }
  return 0;
}

int ptsh_exit(int argc,char **args)
{
  return 1;
}

int ptsh_launch(int argc,char **argv)
{
  FRESULT fr;
  FILINFO fno;
  fr = f_stat(argv[0], &fno);
  if (fr == FR_OK)
  {

    app_intepret(argc, argv);
  } else {
    printf("Bad Command or Filename: %s\n\r",argv[0]);
  }
  return 0;
}

int ptsh_execute(int argc, char **args)
{
  int i;

  if (args[0] == NULL) {
    // An empty command was entered.
    return 0;
  }

  for (i = 0; i < ptsh_num_builtins(); i++) {
    if (strcmp(args[0], builtin_str[i]) == 0) {
      return (*builtin_func[i])(argc,args);
    }
  }

  return ptsh_launch(argc,args);
}

#define PTSH_TOK_BUFSIZE 64
#define PTSH_TOK_DELIM " \t\r\n\a"
char **ptsh_split_line(char *line, int *count)
{
  int bufsize = PTSH_TOK_BUFSIZE, position = 0;
  char **tokens = malloc(bufsize * sizeof(char*));
  char *token, **tokens_backup;

  if (!tokens) {
    puts("ptsh: allocation error");
    return NULL;
  }

  token = strtok(line, PTSH_TOK_DELIM);
  while (token != NULL) {
    tokens[position] = token;
    position++;

    if (position >= bufsize) {
      bufsize += PTSH_TOK_BUFSIZE;
      tokens_backup = tokens;
      tokens = realloc(tokens, bufsize * sizeof(char*));
      if (!tokens) {
		free(tokens_backup);
        puts("ptsh: allocation error");
        return NULL;
      }
    }

    token = strtok(NULL, PTSH_TOK_DELIM);
  }
  tokens[position] = NULL;
  *count = position;
  return tokens;
}

void ptsh_loop(void)
{
  char *line;
  char **args;
  int status = 0;
  int ret = 0;
  int argc = 0;

  do {
    char prompt[26];
    f_getcwd(prompt, 23);
    strncat(prompt," > ", 26);
    // printf("> ");
    line = readline(prompt);
    args = ptsh_split_line(line, &argc);
    if (args != NULL) {
      if ((ret = setjmp(ptsh_buffer)) == 0) 
        ret = ptsh_execute(argc, args);
    } else {
      puts("SYSTEM ERROR!");
      return;
    }
    free(line);
    free(args);
  } while (status == 0);
}