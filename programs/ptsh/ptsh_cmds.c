#include <stdlib.h>
#include <string.h>
#include "pt52io.h"

static const char *FRESULT_str(FRESULT i) {
    switch (i) {
        case FR_OK:
            return "Succeeded";
        case FR_DISK_ERR:
            return "A hard error occurred in the low level disk I/O layer";
        case FR_INT_ERR:
            return "Assertion failed";
        case FR_NOT_READY:
            return "The physical drive cannot work";
        case FR_NO_FILE:
            return "Could not find the file";
        case FR_NO_PATH:
            return "Could not find the path";
        case FR_INVALID_NAME:
            return "The path name format is invalid";
        case FR_DENIED:
            return "Access denied due to prohibited access or directory full";
        case FR_EXIST:
            return "Access denied due to prohibited access (exists)";
        case FR_INVALID_OBJECT:
            return "The file/directory object is invalid";
        case FR_WRITE_PROTECTED:
            return "The physical drive is write protected";
        case FR_INVALID_DRIVE:
            return "The logical drive number is invalid";
        case FR_NOT_ENABLED:
            return "The volume has no work area (mount)";
        case FR_NO_FILESYSTEM:
            return "There is no valid FAT volume";
        case FR_MKFS_ABORTED:
            return "The f_mkfs() aborted due to any problem";
        case FR_TIMEOUT:
            return "Could not get a grant to access the volume within defined "
                   "period";
        case FR_LOCKED:
            return "The operation is rejected according to the file sharing "
                   "policy";
        case FR_NOT_ENOUGH_CORE:
            return "LFN working buffer could not be allocated";
        case FR_TOO_MANY_OPEN_FILES:
            return "Number of open files > FF_FS_LOCK";
        case FR_INVALID_PARAMETER:
            return "Given parameter is invalid";
        default:
            return "Unknown";
    }
}
static int ptsh_argp(int argc, char **argv, char *options[], bool *opt[], uint8_t count){
    for(int k=1;k<argc;k++){
        bool f = false;
        for(int i=0;i<count;i++)
            if(strcmp(options[i],argv[k])==0)
                f = opt[i] = true;
        if(!f) return k;
    }
    return argc;
}


int ptsh_iceberg (int argc, char** argv)    // external app not on disk
{
    printf("Command \eS%s\es\n\r",argv[0]);
    return 0;
}
int ptsh_basic (int argc, char** argv)    // external app not on disk
{
    printf("Command \eS%s\es\n\r",argv[0]);
    return 0;
}

int ptsh_cls (int argc, char** argv)
{
    Home(0);
    Clear_screen(0);
    return 0;
}
int ptsh_mem (int argc, char** argv)
{
    char* free_memory = NULL;
    uint8_t x;
    for (x = 1; x < 250; x++)
    {
        free_memory = malloc(1024 * x);
        if (free_memory == NULL)
        {
            break;
        }
        free(free_memory);
        free_memory = NULL;
    }
    printf ("System Reports Approximately %d K free\n\r", (x - 1));
    return 0;
}
int ptsh_test (int argc, char** argv)
{
    printf("Command \eS%s\es\n\r",argv[0]);
    exit(0);
    return 0;
}
int ptsh_ls (int argc, char** argv)
{
    char cwdbuf[FF_LFN_BUF] = {0};
    FRESULT fr; /* Return value */
    char const *p_dir;

        fr = f_getcwd(cwdbuf, sizeof cwdbuf);
        if (FR_OK != fr) {
            return 1;
        }
        p_dir = cwdbuf;
    printf("Directory of 0:%s\n\r", p_dir);
    DIR dj;      /* Directory object */
    FILINFO fno; /* File information */
    memset(&dj, 0, sizeof dj);
    memset(&fno, 0, sizeof fno);
    fr = f_findfirst(&dj, &fno, p_dir, "*");
    if (FR_OK != fr) {
        return 1;
    }
    uint32_t t_size = 0; uint16_t t_files = 0, t_dirs = 0;
    while (fr == FR_OK && fno.fname[0]) { /* Repeat while an item is found */
        /* Create a string that includes the file name, the file size and the
         attributes string. */
        const char *pcWritableFile = "writable file",
                   *pcReadOnlyFile = "read only file",
                   *pcDirectory = "directory";
        const char *pcAttrib;
        uint8_t fileColour= 047;
        /* Point pcAttrib to a string that describes the file. */
        if (fno.fattrib & AM_DIR) {
            pcAttrib = pcDirectory;
            fileColour = 054;
            t_dirs++;
        } else if (fno.fattrib & AM_RDO) {
            pcAttrib = pcReadOnlyFile;
            fileColour = 041;
            t_files++;
        } else {
            pcAttrib = pcWritableFile;
            fileColour = 052;
            t_files++;
        }
        /* Create a string that includes the file name, the file size and the
         attributes string. */
        printf("\ef%c%-15s\ed %10u byte(s)\n\r",fileColour, fno.fname, fno.fsize);
        t_size += fno.fsize;
        fr = f_findnext(&dj, &fno); /* Search for next item */
    }
    printf("%5d File(s)   %10u bytes\n\r%5d Dir(s)\n\r", t_files, t_size, t_dirs);
    f_closedir(&dj);
    return 0;
}
int ptsh_type (int argc, char** argv)
{
    FIL fil;
    FRESULT fr = f_open(&fil, argv[1], FA_READ);
    if (FR_OK != fr) {
        printf("\ef041\eR error: %s Not found or not Accessable\er\ed\n\r", argv[1]);
        return 1;
    }
    putchar('\e');putchar('N');
    char buf[256];
    while (f_gets(buf, sizeof buf, &fil)) {
        printf("%s", buf);
    }
    fr = f_close(&fil);
    putchar('\e');putchar('n');
    if (FR_OK != fr) { printf("\ef041\eR error: %s (%d)\er\ed\n\r", argv[1]);return 1;}
    return 0;
}
int ptsh_dummy (int argc, char** argv)
{
    printf("Command \eS%s\es\n\r",argv[0]);
    printf ("%d:",argc);
    for (int i = 0; argv[i] != NULL; i++)
    {
        printf ("%s ", argv[i]);
    }
    puts('\0');
    return 0;
}
int ptsh_ver (int argc, char** argv)
{
    printf ("ptsh version %s\n\r", PTSH_VERSION);
    return 0;
}
int ptsh_cd(int argc, char** argv) {
    FRESULT fr;
    if (argc != 2) {
    char str[80];
        fr = f_getcwd(str, 80);
        if (FR_OK != fr) return 1;
        puts(str);
        return 0;
    }
    fr = f_chdir(argv[1]);
    if (FR_OK != fr) return 1;
    return 0;
}
int ptsh_mkdir(int argc, char** argv) {
    if (argc != 2) {
        printf("mkdir: missing operand\n\r");
        return 1;
    }
    FRESULT fr = f_mkdir(argv[1]);
    if (FR_OK != fr) {
        printf("mkdir: error %s (%d)\n\r", FRESULT_str(fr), fr);
        return 1;
    }
    return 0;
}
int ptsh_move(int argc, char** argv) {
    if (argc != 3) {
        puts("mv: missing file operand");
        return 1;
    }
    FRESULT fr = f_rename (argv[1], argv[2]);
    if (FR_OK != fr) {
        printf("move: error %s (%d)\n\r", FRESULT_str(fr), fr);
        return 1;
    }
    return 0;
}
int ptsh_copy(int argc, char **argv)
{
    FILE *fp1;
    FILE *fp2;

    if (argc != 3)
        puts("copy: missing file operand");
    if ((fp1 = fopen(argv[1], "rb")) == 0)
        printf("cannot open file %s for reading\n\r", argv[1]);
    if ((fp2 = fopen(argv[2], "wb")) == 0)
        printf("cannot open file %s for writing\n\r", argv[2]);
    char            buffer[512];
    size_t          n;

    while ((n = fread(buffer, sizeof(char), sizeof(buffer), fp1)) > 0)
    {
        if (fwrite(buffer, sizeof(char), n, fp2) != n)
            printf("write failed\n\r");
    }
    fclose(fp1);
    fclose(fp2);

    return 0;
}
int ptsh_del(int argc, char **argv)
{
    if (argc != 2){
        puts("rm: missing operand");
        return 1;
    }
    FRESULT fr = f_unlink (argv[1]);
    if (FR_OK != fr){
        printf("rm: error %s (%d)\n\r", FRESULT_str(fr), fr);
        return 1;
    }
    return 0;
}

int ptsh_echo(int argc, char **argv)
{
    for (int i = 1; i < argc; i++)
        printf("%s ",argv[i]);
    puts('\0');
}
#if 0
void ls(const char *dir) {
    char cwdbuf[FF_LFN_BUF] = {0};
    FRESULT fr; /* Return value */
    char const *p_dir;
    if (dir[0]) {
        p_dir = dir;
    } else {
        fr = f_getcwd(cwdbuf, sizeof cwdbuf);
        if (FR_OK != fr) {
            printf("f_getcwd error: %s (%d)\n\r", FRESULT_str(fr), fr);
            return;
        }
        p_dir = cwdbuf;
    }
    printf("Directory Listing: %s\n\r", p_dir);
    DIR dj;      /* Directory object */
    FILINFO fno; /* File information */
    memset(&dj, 0, sizeof dj);
    memset(&fno, 0, sizeof fno);
    fr = f_findfirst(&dj, &fno, p_dir, "*");
    if (FR_OK != fr) {
        printf("f_findfirst error: %s (%d)\n\r", FRESULT_str(fr), fr);
        return;
    }
    while (fr == FR_OK && fno.fname[0]) { /* Repeat while an item is found */
        /* Create a string that includes the file name, the file size and the
         attributes string. */
        const char *pcWritableFile = "writable file",
                   *pcReadOnlyFile = "read only file",
                   *pcDirectory = "directory";
        const char *pcAttrib;
        /* Point pcAttrib to a string that describes the file. */
        if (fno.fattrib & AM_DIR) {
            pcAttrib = pcDirectory;
        } else if (fno.fattrib & AM_RDO) {
            pcAttrib = pcReadOnlyFile;
        } else {
            pcAttrib = pcWritableFile;
        }
        /* Create a string that includes the file name, the file size and the
         attributes string. */
        printf("%s [%s] [size=%llu]\n\r", fno.fname, pcAttrib, fno.fsize);

        fr = f_findnext(&dj, &fno); /* Search for next item */
    }
    f_closedir(&dj);
}
static void run_ls() {
    char *arg1 = strtok(NULL, " ");
    if (!arg1) arg1 = "";
    ls(arg1);
}
#endif