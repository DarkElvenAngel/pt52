#include <stdlib.h>
#include <ctype.h>
#include "pt52io.h"

#if 0
bool test_task(struct repeating_timer *t)
{
   static bool index;
   const byte bix[16] = { 0x00, 0x7e, 0x42, 0x42, 0x42, 0x42, 0x7e, 0x00, 0xff, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0xff }; 
   Define_User_Char('a', &bix[index * 8]);
   index -=1;
   return true;
}
#endif
int app_test () {
    //struct repeating_timer timer;
    //add_repeating_timer_ms(250, test_task, NULL, &timer);
    //puts("\017a\016");
    Reset_Character_Buffer();
    //Use_FontSet(FS_VT52);
    printf (" FONT SET \r\n");
    for (char x = FONTSET_START; x < FONTSET_START + FONTSET_SIZE; x++)
    {
        printf("%c\eD\eB\017%c\016\eA ",x,x);
    }
    puts("\r\n\n\n[ PRESS 0 - 4  SWITCH FONT SET  (Q)uit]");
    char p;
    do
    {
        p = tolower(getchar());
        Use_FontSet(atoi(&p) % MAX_FONTSETS);
    } while (p != 'q');
    //cancel_repeating_timer(&timer);
    return(0);
    }