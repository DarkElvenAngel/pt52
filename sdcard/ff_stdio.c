/*  ff_stdio.c  -   Use FatFS library to emulate stdio.h routines */

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "ff_stdio.h"
#include <fatfs/ff.h>
#include "pt52io.h"

#ifdef ALT_PRINTF
void ALT_PRINTF (const char *ps);
#endif
void fatal (const char *ps){ puts(ps);}

// #define DEBUG

static FATFS   vol;

void fio_mount (void)
    {
#ifdef DEBUG
    printf ("Mount FatFS\n\r");
#endif
    FRESULT fr = f_mount (&vol, "0:", 0);
    if ( fr != FR_OK )
        {
        char sMsg[80];
        sprintf (sMsg, "Failed to mount SD Card: FR = %d", fr);
        fatal (sMsg);
        }
#ifdef DEBUG
    printf ("fr = %d\n\r", fr);
#endif
    }

static const char * MakeString (const char *fmt, va_list va)
    {
    static char sErr[] = "No space";
    static char *psBuff = NULL;
    static int nBuff = 0;
    if ( psBuff == NULL )
        {
        nBuff = 256;
        psBuff = (char *) malloc (nBuff);
        if ( psBuff == NULL ) return sErr;
        }
    int nCh = vsnprintf (psBuff, nBuff, fmt, va);
    if ( nCh >= nBuff )
        {
        free (psBuff);
        nBuff = nCh + 1;
        psBuff = (char *) malloc (nBuff);
        if ( psBuff == NULL ) return sErr;
        vsnprintf (psBuff, nBuff, fmt, va);
        }
    return (const char *) psBuff;
    }

FILE * fopen (const char *psPath, const char *psMode)
    {
    unsigned char mode = 0;
    FIL *pf = NULL;
#ifdef DEBUG
    printf ("fopen (%s, %s)\n\r", psPath, psMode);
#endif
    while ( *psMode )
        {
        switch (*psMode)
            {
            case 'r':
                mode |= FA_READ;
                break;
            case 'w':
                mode |= FA_CREATE_ALWAYS | FA_WRITE;
                break;
            case '+':
                mode |= FA_READ | FA_WRITE;
                break;
            case 'a':
                mode &= ~FA_CREATE_ALWAYS;
                mode |= FA_OPEN_APPEND | FA_WRITE;
                break;
            case 'x':
                mode |= FA_CREATE_NEW;
                break;
            default:
                break;
            }
        ++psMode;
        }
#ifdef DEBUG
    printf ("mode - %d\n\r", mode);
#endif
    pf = (FIL *) malloc (sizeof (FIL));
    if ( pf == NULL )
        {
#ifdef DEBUG
        printf ("Failed to allocate buffer\n\r");
#endif
        return pf;
        }
    FRESULT fr = f_open (pf, psPath, mode);
    if ( fr != FR_OK )
        {
#ifdef DEBUG
        printf ("Failed\n\r");
#endif
        free (pf);
        pf = NULL;
        }
#ifdef DEBUG
    printf ("fr = %d, pf = %p\n\r", fr, pf);
#endif
    return pf;
    }

int fclose (FILE *pf)
    {
#ifdef DEBUG
    printf ("fclose (%p)\n\r", pf);
#endif
    if ( ( pf == NULL ) || ( pf == stdout ) || ( pf ==stderr ) ) return 0;
    FRESULT fr = f_close ((FIL *) pf);
#ifdef DEBUG
    printf ("Close fr = %d\n\r");
#endif
    if ( fr != FR_OK ) return EOF;
    free (pf);
    return 0;
    }

int fputc (char ch, FILE *pf)
    {
    if ( ( pf == NULL ) || ( pf == stdout ) || ( pf ==stderr ) ) return EOF;
    if ( f_putc (ch, (FIL *) pf) != FR_OK ) return EOF;
    return (int)(unsigned char) ch;
    }

int fputs (const char *ps, FILE *pf)
    {
    if ( pf == NULL ) return EOF;
    else if ( ( pf == stdout ) || ( pf ==stderr ) )
        {
#ifdef ALT_PRINTF
        ALT_PRINTF (ps);
#else
        printf ("%s", ps);
#endif
        }
    else if ( f_puts (ps, (FIL *) pf) != FR_OK ) return EOF;
    return 1;
    }

char *fgets (char *ps, int size, FILE *pf)
    {
    return f_gets (ps, size, (FIL *)pf);
    }

size_t fread (void *ptr, size_t size, size_t nmemb, FILE *pf)
    {
    size_t in;
#ifdef DEBUG
    printf ("fread (%p, %d, %d, %p)\n\r", ptr, size, nmemb, pf);
#endif
    FRESULT fr = f_read ((FIL *) pf, ptr, size * nmemb, &in);
#ifdef DEBUG
    printf ("Read fr = %d, in = %d\n\r", fr, in);
#endif
    if ( fr != FR_OK ) return EOF;
    return in / size;
    }

size_t fwrite (const void *ptr, size_t size, size_t nmemb, FILE *pf)
    {
    size_t out;
#ifdef DEBUG
    printf ("fwrite (%p, %d, %d, %p)\n\r", ptr, size, nmemb, pf);
#endif
    if ( ( pf == NULL ) || ( pf == stdout ) || ( pf ==stderr ) ) return EOF;
    FRESULT fr = f_write ((FIL *) pf, ptr, size * nmemb, &out);
#ifdef DEBUG
    printf ("Write fr = %d, out = %d\n\r", fr, out);
#endif
    if ( fr != FR_OK ) return EOF;
    return out / size;
    }

int fprintf (FILE *pf, const char *fmt, ...)
    {
    int nByte;
    va_list va;
    va_start (va, fmt);
    nByte = vfprintf (pf, fmt, va);
    va_end (va);
    return nByte;
    }

int vfprintf (FILE *pf, const char *fmt, va_list va)
    {
    size_t out;
    const char *sOut;
    if ( pf == NULL ) return EOF;
    sOut = MakeString (fmt, va);
    if ( ( pf == stdout ) || ( pf ==stderr ) )
        {
        out = strlen (sOut);
#ifdef ALT_PRINTF
        ALT_PRINTF (sOut);
#else
        printf ("%s", sOut);
#endif
        }
    else
        {
        f_write ((FIL *) pf, sOut, strlen (sOut), &out);
        }
    return out;
    }

long ftell (FILE *pf)
    {
    return f_tell ((FIL *)pf);
    }

int fseek (FILE *pf, long offset, int whence)
    {
    if ( whence == SEEK_END )
        {
        offset += f_size ((FIL *)pf);
        }
    else if ( whence == SEEK_CUR )
        {
        offset += f_tell ((FIL *)pf);
        }
    else if ( whence != SEEK_SET )
        {
        return -1;
        }
    if ( f_lseek ((FIL *)pf, offset) != FR_OK ) return -1;
    return 0;
    }

int fflush (FILE *pf)
	{
	return 0;
	}

int remove (const char *psPath)
    {
    if ( f_unlink (psPath) != FR_OK ) return -1;
    return 0;
    }

int rename (const char *psOld, const char *psNew)
    {
    if ( f_rename (psOld, psNew) != FR_OK ) return -1;
    return 0;
    }

static int iUngetCh = -1;

int getc (FILE *pf)
    {
    int iCh = iUngetCh;
    if ( iCh >= 0 )
        {
        iUngetCh = -1;
        }
    else
        {
        size_t in;
        iCh = 0;
        f_read ((FIL *) pf, &iCh, 1, &in);
        if ( in != 1 ) iCh = EOF;
        }
    return iCh;
    }

int ungetc (int iCh, FILE *pf)
    {
    int iRes = iCh;
    if ( iUngetCh != -1 ) iRes = EOF;
    iUngetCh = iCh;
    return iRes;
    }

ssize_t getline(char **line, size_t *len, FILE *fp)
{
    if(line == NULL || len == NULL || fp == NULL) {
         
         return -1;
     }
     
     // Use a chunk array of 128 bytes as parameter for fgets
     char chunk[128];
 
     // Allocate a block of memory for *line if it is NULL or smaller than the chunk array
     if(*line == NULL || *len < sizeof(chunk)) {
         *len = sizeof(chunk);
         if((*line = malloc(*len)) == NULL) {
             
             return -1;
         }
     }
 
     // "Empty" the string
     (*line)[0] = '\0';
 
     while(fgets(chunk, sizeof(chunk), fp) != NULL) {
         // Resize the line buffer if necessary
         size_t len_used = strlen(*line);
         size_t chunk_used = strlen(chunk);
 
         if(*len - len_used < chunk_used) {
             // Check for overflow
             if(*len > SIZE_MAX / 2) {
                 
                 return -1;
             } else {
                 *len *= 2;
             }
             
             if((*line = realloc(*line, *len)) == NULL) {
                 
                 return -1;
             }
         }
 
         // Copy the chunk to the end of the line buffer
         memcpy(*line + len_used, chunk, chunk_used);
         len_used += chunk_used;
         (*line)[len_used] = '\0';
 
         // Check if *line contains '\n', if yes, return the *line length
         if((*line)[len_used - 1] == '\n') {
             return len_used;
         }
     }
 
     return -1;
  
}

int F_truncate(FILE *fp, off_t length)
{
    if (fp == NULL) return -1;
    FRESULT t = f_lseek(fp,length);
    if (t != 0) return -1;
    t = f_truncate(fp);
    if (t != 0) return -1;
    f_rewind(fp);
    return t;
}

static FIL *PT_FilePool[MAX_FILE_DESCRIPTORS] = { 0 };

size_t PT_write (int fd, void* buf, size_t cnt)
{
    if (PT_FilePool[fd] == NULL) return -1;
    size_t ret = 0;  
    FRESULT fr = f_write (PT_FilePool[fd], buf, cnt, &ret);
    if ( fr != FR_OK ) return -1;
    return ret;
} 
size_t PT_read (int fd, void* buf, size_t cnt)
{
    if (PT_FilePool[fd] == NULL) return -1;
    size_t ret = 0;  
    FRESULT fr = f_read (PT_FilePool[fd], buf, cnt, &ret);
    if ( fr != FR_OK ) return -1;
    return ret;
}  
int PT_close(int fd)
{
    if (PT_FilePool[fd] == NULL) return -1;
    FRESULT fr = f_close ((FIL *) PT_FilePool[fd]);
    if ( fr != FR_OK ) return -1;
    free (PT_FilePool[fd]);
    PT_FilePool[fd] = NULL;
    return 0;
}
int PT_open (const char* Path, int flags )
{
    int fd = -1;
    for (int i = 0; i < MAX_FILE_DESCRIPTORS; i++)
        if (PT_FilePool[i] == NULL){
            fd = i;
            break;
        } 
    if (fd == -1) return -1;  // Out of file descriptors

    PT_FilePool[fd] = (FIL *) malloc (sizeof (FIL));
    if ( PT_FilePool[fd] == NULL ) 
    {
        return -1;
    }
    FRESULT fr = f_open (PT_FilePool[fd], Path, flags);
    if ( fr != FR_OK )
    {
        printf("ERROR: %d unable to open %s with fd %d\n\r", fr, Path, fd);
        free (PT_FilePool[fd]);
        PT_FilePool[fd] = NULL;
        return -1;
    }
    return fd;
}
