/*  stdio.h  -   Use FatFS library to emulate stdio.h routines */

#ifndef H_FF_STDIO
#define H_FF_STDIO

#include <stdio.h>
#include "fatfs/ff.h"
#undef  ftruncate
#undef  rewind
#undef  feof
#undef  open
#undef  close
#undef  read
#undef  write


#define fopen       fio_fopen
#define fclose      fio_fclose
#define fputc       fio_fputc
#define fputs       fio_fputs
#define fgets       fio_fgets
#define fread       fio_fread
#define fwrite      fio_fwrite
#define fprintf     fio_fprintf
#define vfprintf    fio_vfprintf
#define ftell       fio_ftell
#define fseek       fio_fseek
#define fflush      fio_fflush
#define ftruncate   F_truncate
#define rewind(x)   f_rewind(x)
#define remove      fio_remove
#define rename      fio_rename
#define getc        fio_getc
#define ungetc      fio_ungetc
#define feof(fp)    ((int)(((FIL *)fp)->fptr == ((FIL *)fp)->obj.objsize))
//(FIL *) 
#ifndef NULL
#define NULL    0
#endif
#undef FILE
#define FILE    void
#ifndef EOF
#define EOF     (-1)
#endif
#ifndef SEEK_SET
#define SEEK_SET    0
#define SEEK_CUR    1
#define SEEK_END    2
#endif
#define MAX_FILE_DESCRIPTORS 3
#define O_RDONLY FA_READ
#define O_WRONLY FA_WRITE
#define O_RDWR   (FA_READ | FA_WRITE)
#define O_CREAT  FA_CREATE_ALWAYS
#define O_EXCL   FA_CREATE_NEW

#define  open   PT_open
#define  close  PT_close
#define  read   PT_read
#define  write  PT_write

typedef unsigned int size_t;

#undef stdout
#undef stderr
#define stdout  (void *)(-1)
#define stderr  (void *)(-2)

#ifdef __cplusplus
extern "C"
    {
#endif

void fio_mount ();
FILE * fopen (const char *psPath, const char *psMode);
int fclose (FILE *pf);
int fputc (char ch, FILE *pf);
int fputs (const char *ps, FILE *pf);
char *fgets (char *ps, int size, FILE *pf);
size_t fread (void *ptr, size_t size, size_t nmemb, FILE *pf);
size_t fwrite (const void *ptr, size_t size, size_t nmemb, FILE *pf);
int fprintf (FILE *pf, const char *fmt, ...);
int vfprintf (FILE *pf, const char *fmt, va_list va);
long ftell (FILE *pf);
int fseek (FILE *pf, long offset, int whence);
int fflush (FILE *pf);
int remove (const char *psPath);
int rename (const char *psOld, const char *psNew);
ssize_t getline(char **lineptr, size_t *n, FILE *stream);
size_t PT_write (int fd, void* buf, size_t cnt); 
size_t PT_read (int fd, void* buf, size_t cnt);  
int PT_close(int fd); 
int PT_open (const char* Path, int flags ); 

int ftruncate(FILE *fp, off_t length);
// The following two routines only work reliably on one stream at a time
int getc (FILE *pf);
int ungetc (int iCh, FILE *pf);

#ifdef __cplusplus
    }
#endif



#endif
