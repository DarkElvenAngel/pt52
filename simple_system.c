/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2021, DarkElvenAngel
 * All rights reserved.
 * 
 * see LICENSE for full Licence
 */
#define PICO_MALLOC_PANIC 0
// #include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include "pico.h"
#include "pico/stdlib.h"
#include "pico/scanvideo.h"
#include "pico/scanvideo/composable_scanline.h"
#include "pico/multicore.h"
#include "hardware/clocks.h"
#include "hardware/uart.h"
#include "hardware/irq.h"
#include "video_driver.h"
#include "config.h"

#include "bsp/board.h"
#include "tusb.h"
#include "pico/stdlib.h"
#include "hardware/clocks.h"
#include "charbuffer_improved.h"
#include "bsp/board.h"
#include "tusb.h"
#include "uart.h"
#include "usb-keyboard.h"
#include "simple_system.h"
#include "sdcard/ff_stdio.h"
#include "fatfs/ff.h"
#include "sdcard/dirt.h"
//  Included to clean up vscode errors
#include "class/hid/hid.h"

/*
cmd_t dsp_table[] ={
    CMD(mem,"","check how much memory is free"),
    CMD(hex,"S","Start Hex viewer"),
    CMD(term,"","Start Terminal"),
    CMD(basic,"S","BASIC interpreter"),
    CMD(cls,"","Clear the screen"),
    CMD(test,"","This is for testing functions it has no set use"),
    CMD(iceberg,"","Iceberg, Can you out smart the enemy?"),
    CMD(edit,"S","Text Editor for PT52"),
    CMD(exit,"","Exits the interpreter"),
    CMD(ls,"","SD CARD TESTING FUNCTION WILL BE FOR DIR LIST"),
    CMD(type, "s", "Print contents of file to screen"),
    CMD(dummy, "s", "DUMMY FUNC"),
    CMD(help,"","Display this help"),
    { NULL, NULL, NULL },
};
*/    
static char last_char = 0;
static keycode_t last_keycode = { 0 };
/**
 * Read formatted input from keyboard.
 * 
 * \param format controls the output as in C printf.
 * \return the number of input items successfully matched and assigned
 */
int Scanf(const char *format, ...)
{
    char* s = readline(NULL);
    va_list args;
    va_start(args, format);
    int r = vsscanf(s, format, args);
    va_end(args);
    free(s);
    return r;
}
/**
 * Get Line from the user.  /!\ IMPORTANT free the returned pointer!
 * 
 * \param const char* prompt what prompt you want to use can be NULL
 * \return char* returns the text of the line read. A blank line returns the empty string.
 */
char *readline(const char* prompt)
{
    // This is a blocking call
    if (prompt != NULL) printf(prompt);     // print the prompt if one is given
    char* ret = (char*)malloc(160 * sizeof(char));   // allocate memory for the return data
    if (ret == NULL)
    {
        printf("\r\n\eSERROR!  Unable to allocate memory for readline!\es\r\n");
        return NULL;
    }
    memset(ret,0,160 *sizeof(char));
    last_char = 0;                          // reset the last_char so we don't read old data
        uint8_t c = 0;                      // keep track of the number of characters are entered
    while (last_char != '\r')               // note that last_char is updated in the Key_pressed function
    {
        if (last_char >= ' ')               // new data is avaliable and is not the end line.
        {
            ret[c++] = last_char;           // add new character to the return increment the character counter
            putchar(last_char);
            last_char = 0;                  // reset last_char
        }
        if (last_char == 0x08 && c > 0 )    // Backspace
        {
            Backspace();
            c--;                            // decrement c 
            last_char = 0;                  // reset last_char 
        }
        if (c >= 160 - 2) break;                 // if the buffer is almost full exit loop early.
        sleep_ms(1);
    }
    Enter();
    return ret;                             // return the pointer 
}

/**
 * Get characater 
 * 
 * \return char returns the last character pressed.
 */
char Getchar()
{
    last_char = 0;
    while (last_char == 0)
    {
        sleep_ms(1);
    }
    return last_char;
}

/**
 * Get Last Keypress
 * 
 * \return keycode_t
 */
keycode_t get_lastkey()
{
    last_keycode = (keycode_t){ 0 };
    while (last_keycode.keycode == 0)
    {
        sleep_ms(1);
    }
    return last_keycode;
}

//  This is the replacement for simple_terminal and vga_terminal
uint8_t const keycode2ascii[128][3] = {HID_KEYCODE_TO_ASCII_WITH_CTRL};
/**
 * Key Pressed Event
 * 
 * \param Ctrl is the control key pressed
 * \param Shift is the shift key pressed
 * \param keycode 
 * \return none 
 */
void Key_Pressed(bool Ctrl, bool Shift, uint8_t keycode)
{
    bool LockKey_Caps = keylock_flags & KEYBOARD_LED_CAPSLOCK;
    bool LockKey_Num = keylock_flags & KEYBOARD_LED_NUMLOCK;
    if (LockKey_Caps && ! Ctrl) if (keycode >= 0x04 && keycode <= 0x1D ) Shift -= 1;
    if (LockKey_Num && ! Ctrl) if (keycode >= 0x59 && keycode <= 0x63 ) Shift -= 1;
    static bool Local_Tools = false;
    uint8_t ch = keycode2ascii[keycode][(Shift && !Ctrl) ? 1 : 0 + Ctrl ? 2 : 0];
    last_keycode.shift = Shift;
    last_keycode.ctrl = Ctrl;
    last_keycode.character = ch;
    last_keycode.keycode = keycode;
    switch (ch)
    {
        case 0: switch (keycode) // non-printable characters
        {
            //case 0x1F: if (Ctrl) uart_putc_raw(UART_ID, 0);break;   // CTRL @ send NULL
            //case 0x44: break;                                       // F11 key (Raw input mode) 
            case 0x45: Local_Tools -= 1;                            // F12 key [LOCAL ECHO]
            /*case 0x46: Reset_Character_Buffer(); break;             // PRINT SCREEN
            case 0x48: if (Ctrl) uart_puts(UART_ID, "PT52\n");
            case 0x4c: uart_putc_raw(UART_ID, 127);break;           // Delete key
            case 0x4f: uart_puts(UART_ID, "\eC"); break;            // cursorX += cursorX <= COLS-1 ? 1 : 0; break;// right
            case 0x50: uart_puts(UART_ID, "\eD"); break;            // cursorX -= cursorX > 0 ? 1 : 0; break;// left
            case 0x51: uart_puts(UART_ID, "\eB"); break;            // cursorY += cursorY <= FOOTER - 2 ? 1 : 0; break;// down
            case 0x52: uart_puts(UART_ID, "\eA"); break;            // cursorY -= cursorY > HEADER ? 1 : 0; break;//up
            */
            //case 0x65: Setup_Menu(); break;                       // Menu Key
            default   :  //Decode_Character(ch); break;//uart_putc_raw(UART_ID, ch);break;
            if (Local_Tools)
            {
                Home(0);
                printf ("PRESS A KEY");
                Move_cursorto(0,2);
                printf("KEYCODE [0x%02X] \eK%s %s %0x", keycode, Ctrl ? "[CTRL]" : "", Shift ? "[SHIFT]" : "", ch);
            }
        } 
        break; 
        //case 0x08 : uart_putc_raw(UART_ID, ch);break;             // BackSpace 
        //case 0x09 : uart_putc_raw(UART_ID, ch);break;             // Tab Key
        //case 0x1b : uart_putc_raw(UART_ID, ch);break;             // Escape key
        //case '\r' : uart_puts(UART_ID, "\n\r");break;             // ENTER []
        default   :  //Decode_Character(ch); break;//uart_putc_raw(UART_ID, ch);break;
            if (Local_Tools)
            {
                Home(0);
                printf ("PRESS A KEY");
                Move_cursorto(0,2);
                printf("KEYCODE [0x%02X] \eK%s %s %02X", keycode, Ctrl ? "[CTRL]" : "", Shift ? "[SHIFT]" : "", ch);
            } else {
                // Decode_Character(ch);//uart_putc_raw(UART_ID, ch);
                // if (ch > ' ') Cmd_String[Cmd_Index++] = ch;
                last_char = ch;
            }
            break;
   }
}
bool Keyboard_tasks(struct repeating_timer *t)
{
        tuh_task();
        led_blinking_task();
        //hid_task();
        //hid_app_task();
        return true;
}
/*
extern int app_hexview(char* filename);// int app_hexview();
extern int app_term();
extern int app_iceberg();
extern int app_test();
extern int app_basic();
extern int app_kilo();
const char *delim = " \n";
#define ESCAPE {free(args); puts("Bad Argument(s)"); return NULL;}
arg_t *args_parse(const char *s)
{
#if 0 // 0 disable argument 1 enable
    int argc=strlen(s);
    arg_t *args=malloc(sizeof(arg_t)*argc);
    int i;
    for(i=0;i<argc;++i) {
        char *tok;
        switch(s[i]) {
            case 'S':
                args[i].s = strtok(NULL,delim);
                break;
            case 's':
                args[i].s = strtok(NULL,delim);
                if(!args[i].s)
                    ESCAPE;
                break;
            case 'c':
                tok = strtok(NULL,delim);
                if(!tok)
                   ESCAPE;
                args[i].c = tok[0];
                if(!islower(args[i].c))
                   ESCAPE;
                break;
            // case 'f':  // Disable Float Arguments at this time.
            //    tok = strtok(NULL,delim);
            //    if(sscanf(tok,"%f", &args[i].f)!=1)
            //       ESCAPE;
            //    break;
        }
    }
    return args;
#else
    return NULL;
#endif
}
#undef ESCAPE
void parse(char *cmd)
{
    const char* tok = strtok(cmd,delim);
    if(!tok)
        return;
    
    for(uint8_t i = 0;; i++ )
    {
        cmd_t cur = dsp_table[i];
        if (cur.name == NULL) break;
        if(!strcmp(tok,cur.name)) {
            arg_t *args = args_parse(cur.args);
            if(args==NULL && strlen(cur.args))
                return;//Error in argument parsing
            cur.func(args);
            free(args);
            return;
        }
    }

    puts("Command Not Found");
}

*/
void ptsh_loop(); // this is lazy and bad!
int main (void)
{
    set_sys_clock_khz (200000, true);
    board_init();

    tusb_init();
    video_init();

    struct repeating_timer timer[2];
    add_repeating_timer_ms(50, Keyboard_tasks, NULL, &timer[0]);
    add_repeating_timer_ms(250, Blink_task, NULL, &timer[1]);
    Reset_Character_Buffer();
    
    // delay if your display is slow to wake up.
#if 0 // Skip TESTS
    sleep_ms(3000);
    // These are simple and mostly pointless tests
    printf("SYSTEM STARTUP TESTS ...\n\r");
    sleep_ms(500);
    printf("FONT TEST\n\r");
    printf("\esABCDEFGHIGKLMNOPQRSTUVWXYZ`1234567890-=[]\\;',./\n\r\eSABCDEFGHIGKLMNOPQRSTUVWXYZ`1234567890-=[]\\;',./\n\r");
    printf("\esabcdefghijklmnopqrstuvwxyz~!@#$%%^&*()_+{}|:\"<>?\n\r\eSabcdefghijklmnopqrstuvwxyz~!@#$%%^&*()_+{}|:\"<>?\n\r\es");
#if defined (CHARBUFFER_SUPPORT_COLOUR_16) || defined(CHARBUFFER_SUPPORT_COLOUR_256)
    printf("COLOUR TEST : ");
#ifdef CHARBUFFER_SUPPORT_COLOUR_16
    for (int c = 0; c < 16; c++)
    {
        Set_BColour (c);
        printf ("%d",c);
    }
    Set_Colours(DEFAULT_COLOURS);
    printf("\n\r");
#endif
#if defined(CHARBUFFER_SUPPORT_COLOUR_256)
    printf("\n\r\t");
    for (int c = 0; c < 16; c++)
    {
        Set_BColour (c);
        printf (" %02X ",c);
    }
    Set_Colours(DEFAULT_COLOURS);
    printf("\n\r");

    for (int c = 16, i = 0 ; c < 232; c++, i++)
    {
        if (i % 36 == 0) { Set_Colours(DEFAULT_COLOURS); printf("\n\r   ");}
        Set_BColour (c);
        printf ("%02X",c);
    }
    Set_Colours(DEFAULT_COLOURS);
    printf("\n\n\r\t\t");
    for (int c = 232; c < 256; c++)
    {
        Set_BColour (c);
        printf ("%02X",c);
    }
    Set_Colours(DEFAULT_COLOURS);
    printf("\n\r");
#endif
    for (int p = 0; p < 5; p++) { Set_Pallet(p); sleep_ms(500);}
    Set_Pallet(DEFAULT_PALLET);
#else
    printf("MONOCHROME MODE\n\r");
#endif
    sleep_ms(500);
    printf("SCROLL TEST\n\r");
    Scroll(1);
    sleep_ms(500);
    Scroll(0);
    sleep_ms(500);
    //printf("CLEAR SCREEN TEST\n\r");
    //sleep_ms(1000);
    //printf("\eT");
#endif
    //Set_Colours (DEFAULT_COLOURS);
    //Clear_screen(2);
    //Home(0);
    printf("PT52E\n\r%d X %d %s MODE\n\r",
        MAX_COL, MAX_LINE,
#ifdef CHARBUFFER_SUPPORT_COLOUR_16
        "\eS\ef\041C\ef\042O\ef\043L\ef\044O\ef\045U\ef\046R\ef\047\es"
#elif defined(CHARBUFFER_SUPPORT_COLOUR_256)
        "\eS256 \ef\051C\ef\052O\ef\053L\ef\054O\ef\055U\ef\056R\ef\057\es"
#else
        "MONOCHROME"
#endif
    );
    Set_Colours (DEFAULT_COLOURS);
    FATFS   vol;
    FRESULT fr = f_mount (&vol, "0:", 0);
    if ( fr == FR_OK )
    {   
        f_mount (&vol, "0:", 1);
        printf("Drive 0: %.6s\n\r", &"NODISK FAT12  FAT16  FAT 32 EXFAT  "[vol.fs_type * 7]);
        puts("Diskmode - Active");
    }
    // auto exec goes here
    // cmd_help(NULL);

    puts("[SYSTEM READY]");
    // main loop most of the action is all interupt based
    /*
    while (1)
    {
        char *cmd_str = NULL;
        cmd_str = readline("0:/> ");
        if (cmd_str == NULL) break;
        // simple_shell_command(cmd_str);
        parse(cmd_str);
        free(cmd_str);
        cmd_str = NULL;
    }
    */
    ptsh_loop();
    printf("\r\n>> MAIN LOOP HAS ENDED PLEASE RESET! <<");
    do{sleep_ms(1);}while(1);
}
#if 0
int cmd_mem(arg_t *args) {
    char* free_memory = NULL;
    uint8_t x;
    for (x = 1; x < 250; x++)
    {
        free_memory = malloc(1024 * x);
        if (free_memory == NULL)
        {
            break;
        }
        free(free_memory);
        free_memory = NULL;
    }
    printf ("There is about %d K free\n\r", (x - 1));
    return 0;
}
int cmd_hex(arg_t *args) {app_hexview(args->s);return 0;}
int cmd_term(arg_t *args) {app_term();return 0;}
int cmd_test(arg_t *args) {app_test();return 0;}
int cmd_exit(arg_t *args) {exit(0);}
int cmd_cls(arg_t *args) { Home(0); Clear_screen(0); return 0;}
int cmd_iceberg(arg_t *args){app_iceberg();return 0;}
int pcc (char *argv); 
int cmd_basic(arg_t *args) {return pcc (args->s);/*app_basic();return 0;*/}
int cmd_edit(arg_t *args) 
{
    app_kilo(args->s);
    return 0;
}
int cmd_ls(arg_t *args) 
{
    char cwdbuf[FF_LFN_BUF] = {0};
    FRESULT fr; /* Return value */
    char const *p_dir;

        fr = f_getcwd(cwdbuf, sizeof cwdbuf);
        if (FR_OK != fr) {
            return 1;
        }
        p_dir = cwdbuf;
    printf("Directory of 0:%s\n\r", p_dir);
    DIR dj;      /* Directory object */
    FILINFO fno; /* File information */
    memset(&dj, 0, sizeof dj);
    memset(&fno, 0, sizeof fno);
    fr = f_findfirst(&dj, &fno, p_dir, "*");
    if (FR_OK != fr) {
        return 1;
    }
    uint32_t t_size = 0; uint16_t t_files = 0, t_dirs = 0;
    while (fr == FR_OK && fno.fname[0]) { /* Repeat while an item is found */
        /* Create a string that includes the file name, the file size and the
         attributes string. */
        const char *pcWritableFile = "writable file",
                   *pcReadOnlyFile = "read only file",
                   *pcDirectory = "directory";
        const char *pcAttrib;
        uint8_t fileColour= 047;
        /* Point pcAttrib to a string that describes the file. */
        if (fno.fattrib & AM_DIR) {
            pcAttrib = pcDirectory;
            fileColour = 054;
            t_dirs++;
        } else if (fno.fattrib & AM_RDO) {
            pcAttrib = pcReadOnlyFile;
            fileColour = 041;
            t_files++;
        } else {
            pcAttrib = pcWritableFile;
            fileColour = 052;
            t_files++;
        }
        /* Create a string that includes the file name, the file size and the
         attributes string. */
        printf("\ef%c%-15s\ed %10u byte(s)\n\r",fileColour, fno.fname, fno.fsize);
        t_size += fno.fsize;
        fr = f_findnext(&dj, &fno); /* Search for next item */
    }
    printf("%5d File(s)   %10u bytes\n\r%5d Dir(s)\n\r", t_files, t_size, t_dirs);
    f_closedir(&dj);
    return 0;
}
int cmd_type(arg_t *args)
{
    FIL fil;
    FRESULT fr = f_open(&fil, args->s, FA_READ);
    if (FR_OK != fr) {
        printf("\ef041\eR error: %s Not found or not Accessable\er\ed\n\r", args->s);
        return 1;
    }
    putchar('\e');putchar('N');
    char buf[256];
    while (f_gets(buf, sizeof buf, &fil)) {
        printf("%s", buf);
    }
    fr = f_close(&fil);
    putchar('\e');putchar('n');
    if (FR_OK != fr) { printf("\ef041\eR error: %s (%d)\er\ed\n\r", args->s);return 1;}
    return 0;
}
int cmd_dummy(arg_t *args)
{
    printf("\n\rdummy was passed \eR%s\er \n\r",args->s);
    return 0;
}
int cmd_help(arg_t *args)
{
    puts("Available Commands:");
    for(uint8_t i=0;; i++ )
    {
        cmd_t cmd = dsp_table[i];
        if (cmd.name == NULL) break;
        char tmp[100];//Formatting buffer
        snprintf(tmp,100,"%s ",cmd.name,cmd.args);
        printf("%10s\t- %s\r\n",tmp,cmd.doc);
    }
    return 0;
}
#endif