#ifndef SIMPLE_SYSTEM_H
#define SIMPLE_SYSTEM_H

#undef getchar
#define getchar Getchar
#undef scanf
#define scanf Scanf
#pragma pack(1)
typedef struct{
    union {
        struct {
            uint8_t ctrl : 1;
            uint8_t shift : 1;
            uint8_t alt : 1;
            uint8_t wkey : 1;
        };
        uint8_t Flags;
    };
    char character;
    uint8_t keycode;
} keycode_t;

/*
//Type definitions
typedef union {
    char*       s;
    char        c;
    uint32_t    i;
} arg_t;

typedef struct {
    const char* name;
    int (*func)(arg_t*);
    const char* args;
    const char* doc;
} cmd_t;

#define MK_CMD(x) int cmd_ ## x (arg_t*)
//Functions definitions
MK_CMD(mem);
MK_CMD(hex);
MK_CMD(term);
MK_CMD(cls);
MK_CMD(iceberg);
MK_CMD(basic);
MK_CMD(test);
MK_CMD(exit);
MK_CMD(edit);
MK_CMD(ls);
MK_CMD(type);
MK_CMD(help);
MK_CMD(dummy);

#define CMD(func, params, help) {#func, cmd_ ## func, params, help}
*/

/**
 * Read formatted input from keyboard.
 * 
 * \param format controls the output as in C printf.
 * \return the number of input items successfully matched and assigned
 */
int Scanf(const char *format, ...);


/**
 * Get Line from the user.  /!\ IMPORTANT free the returned pointer!
 * 
 * \param const char* prompt what prompt you want to use can be NULL
 * \return char* returns the text of the line read. A blank line returns the empty string.
 */
char *readline(const char* prompt);

/**
 * Get characater 
 * 
 * \return char returns the last character pressed.
 */
char Getchar();

/**
 * Get Last Keypress
 * 
 * \return keycode_t
 */
keycode_t get_lastkey();

/**
 * Key_Pressed Callback for Keyboard function
 * 
 * \param Ctrl - control key
 * \param Shift - shift key
 * \param keycode that needs processing
 * \return void
 */
void Key_Pressed(bool Ctrl, bool Shift, uint8_t keycode);
#endif