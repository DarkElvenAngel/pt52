/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2021, DarkElvenAngel
 * All rights reserved.
 * 
 * see LICENCE for full Licence
 */
// Simple VGA driver can be improved this is about as simple as it gets

#include <stdio.h>
#include "pico.h"
#include "pico/stdlib.h"
#include "pico/scanvideo.h"
#include "pico/scanvideo/composable_scanline.h"
#include "pico/multicore.h"
#include "hardware/irq.h"
#include "font.h"
#include "charbuffer_improved.h"
#include "config.h"
#if defined (CHARBUFFER_SUPPORT_COLOUR_16) || defined (CHARBUFFER_SUPPORT_COLOUR_256)
#include "pallet.h"
#endif


bool Cursor_Blink = true;
bool _Blink = 0;
extern bool led_state;

bool Blink_task(struct repeating_timer *t)
{
    if (Cursor_Blink) _Blink -= 1; else _Blink = 0;
    return true;
}

/**
 * Set Cursor Visiblity
 * 
 * \param visiblity on or off
 * \return none
 */
void Set_Cursor(bool visiblity)
{
    Cursor_Blink = visiblity;
    if (!visiblity) _Blink = 0;
}
scanvideo_mode_t vga_text_mode =
{
    .default_timing = &vga_timing_640x480_60_default,
    .pio_program = &video_24mhz_composable,
    .width = 640,
    .height = 480,
    .xscale = VGA_X_SCALING,
    .yscale = VGA_Y_SCALING,
};

void __time_critical_func(render_loop) ()
{
    byte *Font = font_reg;
    while (true)
    {
        struct scanvideo_scanline_buffer *buffer = scanvideo_begin_scanline_generation (true);
        uint16_t *pixel_pointer = (uint16_t *) buffer->data;
        int Current_Line = (uint16_t)buffer->scanline_id / GLYPH_HEIGHT;
        uint16_t Scanline = (uint16_t)buffer->scanline_id - (GLYPH_HEIGHT * Current_Line);
#if VGA_FIRST_LINE > 0
        Current_Line -= VGA_FIRST_LINE ;
#endif
        pixel_pointer += 1;
        if (Current_Line  < 0 || Current_Line >= MAX_LINE)
        {
            buffer->data[0] = COMPOSABLE_RAW_1P;
            buffer->data[1] = COMPOSABLE_EOL_SKIP_ALIGN;
            buffer->data_used = 2; 
        } else {
        for ( int Current_Column = 0; Current_Column < MAX_COL; ++Current_Column ) 
        {
            uint8_t character = 0;
#ifdef SIMPLE_CHARBUFFER        // No Attributes or colour
            if (Current_Line < MAX_LINE) character = char_buffer[Current_Line][Current_Column];
            uint16_t fg = 0x2BF;
            uint16_t bg = 0;
#endif
#ifdef FULL_CHARBUFFER        // Attributes enabled
            if (Current_Line < MAX_LINE) character = char_buffer[Current_Line][Current_Column].character;
            Font = (char_buffer[Current_Line][Current_Column].bold) ? font_bold : font_reg;
            if (char_buffer[Current_Line][Current_Column].extended)
            {
                Font = USR_FONT;
            }
#if defined(CHARBUFFER_SUPPORT_COLOUR_16)
            uint16_t fg = Pallets[Cursor.Pallet][char_buffer[Current_Line][Current_Column].foreground_colour];
            uint16_t bg = Pallets[Cursor.Pallet][char_buffer[Current_Line][Current_Column].background_colour];
#if 0   // OPTIMIZED This is now done in the attribute setting and colour setting
            if (char_buffer[Current_Line][Current_Column].reverse)
            {
                bg = fg;
                fg = Pallets[Cursor.Pallet][char_buffer[Current_Line][Current_Column].background_colour];
            }
#endif
#elif defined(CHARBUFFER_SUPPORT_COLOUR_256)
            uint16_t fg = Pallets_256[Cursor.Pallet & 0][char_buffer[Current_Line][Current_Column].foreground_colour];
            uint16_t bg = Pallets_256[Cursor.Pallet & 0][char_buffer[Current_Line][Current_Column].background_colour];
#else
            uint16_t fg = 0x7FFF;;
            uint16_t bg = 0;
#endif
#endif
            uint8_t bits = Font[8 * character + Scanline];
#ifdef FULL_CHARBUFFER        // Attributes enabled
            if (char_buffer[Current_Line][Current_Column].underline && Scanline == 7) bits = 0xff;
#endif
#if !defined(CHARBUFFER_SUPPORT_COLOUR_16) && !defined(CHARBUFFER_SUPPORT_COLOUR_256)
            if (Cursor.Line == Current_Line && Cursor.Column == Current_Column && Scanline == 7 && led_state) { bits = 0xff; fg = 0x7FFF; }
#else
            if (Cursor.Line == Current_Line && Cursor.Column == Current_Column && Scanline == 7 && _Blink) { bits = 0xff; fg = Pallets[Cursor.Pallet][15]; }
#endif
            for (uint8_t Bit_ptr = 0x80; Bit_ptr > 0; Bit_ptr = Bit_ptr >> 1)
            {
                ++pixel_pointer;
                *pixel_pointer = ( bits & Bit_ptr ) ? fg : bg;
            }
        }
        ++pixel_pointer;
        *pixel_pointer = 0;
        *pixel_pointer = COMPOSABLE_EOL_ALIGN;
        pixel_pointer = (uint16_t *) buffer->data;
        pixel_pointer[0] = COMPOSABLE_RAW_RUN;
        pixel_pointer[1] = pixel_pointer[2];
        pixel_pointer[2] = MAX_COL * GLYPH_WIDTH - 2;
        buffer->data_used = ( MAX_COL * GLYPH_WIDTH + 4 ) / 2;
        }
        scanvideo_end_scanline_generation (buffer);
    }
}

void video_init ()
{
    scanvideo_setup(&vga_text_mode);
    scanvideo_timing_enable(true);
    multicore_launch_core1(render_loop);
}

