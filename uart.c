/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2021, DarkElvenAngel
 * All rights reserved.
 * 
 * see LICENCE for full Licence
 */

#include <stdio.h>
#include <string.h>
#include "pico.h"
#include "pico/stdlib.h"
#include "hardware/clocks.h"
#include "pico/multicore.h"
#include "hardware/uart.h"
#include "hardware/irq.h"
#include "uart.h"
#include "config.h"

extern struct uart_settings current_uart_setting;
extern struct uart_settings config_uart_setting;

void UART_setup(int baud, uint8_t databits, uint8_t stopbits, uint8_t parity)
{
    config_uart_setting.baudrate = baud;
    config_uart_setting.data_bits = databits;
    config_uart_setting.stop_bits = stopbits;
    config_uart_setting.parity = parity;
}



void UART_init(irq_handler_t Uart_RX) {
    // Set up our UART with a basic baud rate.
    uart_init(UART_ID, 2400);

    gpio_set_function(PICO_DEFAULT_UART_TX_PIN, GPIO_FUNC_UART);
    gpio_set_function(PICO_DEFAULT_UART_RX_PIN, GPIO_FUNC_UART);

    memcpy(&current_uart_setting, &config_uart_setting, sizeof(struct uart_settings));

    current_uart_setting.baudrate = uart_set_baudrate(UART_ID, config_uart_setting.baudrate);

    // Set UART flow control CTS/RTS, we don't want these, so turn them off
    uart_set_hw_flow(UART_ID, false, false);

    // Set our data format
    uart_set_format(UART_ID, current_uart_setting.data_bits, current_uart_setting.stop_bits, current_uart_setting.parity);//  DATA_BITS, STOP_BITS, PARITY); //

    // Turn off FIFO's - we want to do this character by character
    uart_set_fifo_enabled(UART_ID, false);

    // And set up and enable the interrupt handlers
    irq_set_exclusive_handler(UART1_IRQ, Uart_RX);
    irq_set_enabled(UART1_IRQ, true);

    uart_set_irq_enables(UART_ID, true, false);

}

void UART_close()
{
    uart_deinit(UART_ID);
}