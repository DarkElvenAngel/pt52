#include <stdio.h>
#include "pico.h"
#include "pico/stdlib.h"
#include "pico/scanvideo.h"
#include "pico/scanvideo/composable_scanline.h"
#include "pico/multicore.h"
#include "pico/bootrom.h"
#include "hardware/clocks.h"
#include "pico/multicore.h"
#include "hardware/uart.h"
#include "hardware/irq.h"
#include "pico/unique_id.h"
#include "serialterminal.h"
#include "usb-keyboard.h"
#include "uart.h"

#include "bsp/board.h"
#include "tusb.h"

//  Included to clean up vscode errors
#include "class/hid/hid.h"

bool led_state = false;
bool KeyBoard_Mounted = false;
uint8_t keylock_flags = 0;   // This must be in persistent memory (not on call stack)
//--------------------------------------------------------------------+
// USB HID
//--------------------------------------------------------------------+
// uint8_t const keycode2ascii[128][3] = {HID_KEYCODE_TO_ASCII_WITH_CTRL};

// look up new key in previous keys
static inline bool find_key_in_report(hid_keyboard_report_t const *p_report, uint8_t keycode) {
    for (uint8_t i = 0; i < 6; i++) {
        if (p_report->keycode[i] == keycode) return true;
    }

    return false;
}

static inline void process_kbd_report(hid_keyboard_report_t const *p_new_report) {
    static hid_keyboard_report_t prev_report = {0, 0, {0}}; // previous report to check key released

    //------------- example code ignore control (non-printable) key affects -------------//
    for (uint8_t i = 0; i < 6; i++) {
        if (p_new_report->keycode[i]) {
            if (find_key_in_report(&prev_report, p_new_report->keycode[i])) {
                // exist in previous report means the current key is holding
            } else {
                // not existed in previous report means the current key is pressed
                Key_Pressed(p_new_report->modifier & (KEYBOARD_MODIFIER_LEFTCTRL | KEYBOARD_MODIFIER_RIGHTCTRL),
                            p_new_report->modifier & (KEYBOARD_MODIFIER_LEFTSHIFT | KEYBOARD_MODIFIER_RIGHTSHIFT),
                            p_new_report->keycode[i]);
                uint8_t leds = keylock_flags;
                switch (p_new_report->keycode[i])
                {
                    case HID_KEY_NUM_LOCK:
                        leds ^= KEYBOARD_LED_NUMLOCK;
                        set_leds (leds);
                        break;
                    case HID_KEY_CAPS_LOCK:
                        leds ^= KEYBOARD_LED_CAPSLOCK;
                        set_leds (leds);
                        break;
                    case HID_KEY_SCROLL_LOCK:
                        leds ^= KEYBOARD_LED_SCROLLLOCK;
                        set_leds (leds);
                        break;
                    default:
                        break;
                }
            }
        }
        // TODO example skips key released
    }
    prev_report = *p_new_report;
}
#if PICO_SDK_VERSION_MAJOR == 1
#if PICO_SDK_VERSION_MINOR < 2

CFG_TUSB_MEM_SECTION static hid_keyboard_report_t usb_keyboard_report;
void tuh_hid_keyboard_mounted_cb(uint8_t dev_addr) {
    KeyBoard_Mounted = true;
    
    tuh_hid_keyboard_get_report(dev_addr, &usb_keyboard_report);
}

void tuh_hid_keyboard_unmounted_cb(uint8_t dev_addr) {
    KeyBoard_Mounted = false;
    reset_usb_boot(0,0); 
}

// invoked ISR context
void tuh_hid_keyboard_isr(uint8_t dev_addr, xfer_result_t event) {
    (void) dev_addr;
    (void) event;
}


void hid_task(void) {
    uint8_t const addr = 1;

    if (tuh_hid_keyboard_is_mounted(addr)) {
        if (!tuh_hid_keyboard_is_busy(addr)) {
            process_kbd_report(&usb_keyboard_report);
            tuh_hid_keyboard_get_report(addr, &usb_keyboard_report);
        }
    }

}

#elif PICO_SDK_VERSION_MINOR == 2
void hid_task (void)
    {
    }

// Each HID instance can has multiple reports

#define MAX_REPORT  4
static uint8_t kbd_addr;
static uint8_t _report_count;
static tuh_hid_report_info_t _report_info_arr[MAX_REPORT];

//--------------------------------------------------------------------+
// TinyUSB Callbacks
//--------------------------------------------------------------------+

// Invoked when device with hid interface is mounted
// Report descriptor is also available for use. tuh_hid_parse_report_descriptor()
// can be used to parse common/simple enough descriptor.
void tuh_hid_mount_cb(uint8_t dev_addr, uint8_t instance, uint8_t const* desc_report, uint16_t desc_len)
    {
    // Interface protocol
    uint8_t const interface_protocol = tuh_hid_interface_protocol(dev_addr, instance);
    if ( interface_protocol == HID_ITF_PROTOCOL_KEYBOARD )
        {
        kbd_addr = dev_addr;
#if DEBUG == 1
        printf ("Keyboard mounted: dev_addr = %d\n", dev_addr);
#endif
    
        _report_count = tuh_hid_parse_report_descriptor(_report_info_arr, MAX_REPORT,
            desc_report, desc_len);
#if DEBUG == 1
        printf ("%d reports defined\n", _report_count);
#endif
        }
    }

// Invoked when device with hid interface is un-mounted
void tuh_hid_umount_cb(uint8_t dev_addr, uint8_t __attribute__((unused)) instance)
    {
#if DEBUG == 1
    printf ("Device %d unmounted\n");
#endif
    if ( dev_addr == kbd_addr )
        {
        kbd_addr = 0;
        }
    }

// Invoked when received report from device via interrupt endpoint
void tuh_hid_report_received_cb(uint8_t dev_addr, uint8_t __attribute__((unused)) instance,
    uint8_t const* report, uint16_t len)
    {
    if ( dev_addr != kbd_addr ) return;

    uint8_t const rpt_count = _report_count;
    tuh_hid_report_info_t* rpt_info_arr = _report_info_arr;
    tuh_hid_report_info_t* rpt_info = NULL;

    if ( rpt_count == 1 && rpt_info_arr[0].report_id == 0)
        {
        // Simple report without report ID as 1st byte
        rpt_info = &rpt_info_arr[0];
        }
    else
        {
        // Composite report, 1st byte is report ID, data starts from 2nd byte
        uint8_t const rpt_id = report[0];

        // Find report id in the arrray
        for(uint8_t i=0; i<rpt_count; i++)
            {
            if (rpt_id == rpt_info_arr[i].report_id )
                {
                rpt_info = &rpt_info_arr[i];
                break;
                }
            }

        report++;
        len--;
        }

    if (!rpt_info)
        {
#if DEBUG == 1
        printf("Couldn't find the report info for this report !\r\n");
#endif
        return;
        }

    if ( rpt_info->usage_page == HID_USAGE_PAGE_DESKTOP )
        {
        switch (rpt_info->usage)
            {
            case HID_USAGE_DESKTOP_KEYBOARD:
                // Assume keyboard follow boot report layout
                process_kbd_report( (hid_keyboard_report_t const*) report );
                break;

            default:
                break;
            }
        }
    }

#elif PICO_SDK_VERSION_MINOR == 3
void hid_task (void)
    {
    }

//--------------------------------------------------------------------+
// TinyUSB Callbacks
//--------------------------------------------------------------------+

// Each HID instance can has multiple reports
#define MAX_REPORT  4
static struct
    {
    uint8_t report_count;
    tuh_hid_report_info_t report_info[MAX_REPORT];
    } hid_info[CFG_TUH_HID];

// Invoked when device with hid interface is mounted
// Report descriptor is also available for use. tuh_hid_parse_report_descriptor()
// can be used to parse common/simple enough descriptor.
// Note: if report descriptor length > CFG_TUH_ENUMERATION_BUFSIZE, it will be skipped
// therefore report_desc = NULL, desc_len = 0
void tuh_hid_mount_cb(uint8_t dev_addr, uint8_t instance, uint8_t const* desc_report, uint16_t desc_len)
    {
    uint8_t const itf_protocol = tuh_hid_interface_protocol(dev_addr, instance);
#if DEBUG > 0
    printf("HID device address = %d, instance = %d is mounted\r\n", dev_addr, instance);

    // Interface protocol (hid_interface_protocol_enum_t)
    const char* protocol_str[] = { "None", "Keyboard", "Mouse" };
    printf("HID Interface Protocol = %s\r\n", protocol_str[itf_protocol]);
#endif

    // By default host stack will use activate boot protocol on supported interface.
    // Therefore for this simple example, we only need to parse generic report descriptor
    // (with built-in parser)
    if ( itf_protocol == HID_ITF_PROTOCOL_NONE )
        {
        hid_info[instance].report_count = tuh_hid_parse_report_descriptor(hid_info[instance].report_info,
            MAX_REPORT, desc_report, desc_len);

        }

    // request to receive report
    // tuh_hid_report_received_cb() will be invoked when report is available
    if ( !tuh_hid_receive_report(dev_addr, instance) )
        {

        }
    }

// Invoked when device with hid interface is un-mounted
void tuh_hid_umount_cb(uint8_t dev_addr, uint8_t instance)
    {

    }

// Invoked when received report from device via interrupt endpoint
void tuh_hid_report_received_cb(uint8_t dev_addr, uint8_t instance, uint8_t const* report, uint16_t len)
    {
    uint8_t const itf_protocol = tuh_hid_interface_protocol(dev_addr, instance);

    if (itf_protocol == HID_ITF_PROTOCOL_KEYBOARD)
        {
        process_kbd_report( (hid_keyboard_report_t const*) report );
        }

    // continue to request to receive report
    if ( !tuh_hid_receive_report(dev_addr, instance) )
        {

        }
    }
#else
#error Unknown SDK Version
#endif  // PICO_SDK_VERSION_MINOR
#endif  // PICO_SDK_VERSION_MAJOR
void led_blinking_task(void) {
    const uint32_t interval_ms = 250;
    static uint32_t start_ms = 0;


    // Blink every interval ms
    if (board_millis() - start_ms < interval_ms) return; // not enough time
    start_ms += interval_ms;
    //if (led_state) set_pwm_pin(28,90,5000); else set_pwm_pin(28,90,0);
    board_led_write(led_state);
    led_state -= 1; //- led_state; // toggle
}

void set_leds (uint8_t leds)
{
    if ( leds != keylock_flags )
    {
    uint8_t const addr = 1;
    keylock_flags = leds;

    tusb_control_request_t ledreq = {
        .bmRequestType_bit.recipient = TUSB_REQ_RCPT_INTERFACE,
        .bmRequestType_bit.type = TUSB_REQ_TYPE_CLASS,
        .bmRequestType_bit.direction = TUSB_DIR_OUT,
        .bRequest = HID_REQ_CONTROL_SET_REPORT,
        .wValue = HID_REPORT_TYPE_OUTPUT << 8,
        .wIndex = 0,    // Interface number
        .wLength = sizeof (keylock_flags)
        };

    tuh_control_xfer (addr, &ledreq, &keylock_flags, NULL);
    }
}
