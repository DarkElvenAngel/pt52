/**
 * USB KEYBOARD - TinyUSB 0.10.x 
 * 
 * This code is mostly just the example code modified to work in this project
 */
#include <stdarg.h>
#include <stdio.h>
#include "pico.h"
#include "usb-keyboard.h"
#include "pt52io.h"

#include "bsp/board.h"
#include "tusb.h"

//  Included to clean up vscode errors
#include "class/hid/hid.h"
bool led_state = false;
uint8_t KeyBoard_Mounted = 0;
uint8_t keylock_flags = 0;   // This must be in persistent memory (not on call stack)

//--------------------------------------------------------------------+
// USB CDC These are called but are they required?
//--------------------------------------------------------------------+
// invoked ISR context This shouldn't be required but for some reason 
// if it's not included the we can't compile *BUG*
void tuh_cdc_xfer_isr(uint8_t dev_addr, ...){}
//void tuh_cdc_xfer_isr(uint8_t dev_addr, xfer_result_t event, cdc_pipeid_t pipe_id, uint32_t xferred_bytes) { }

void led_blinking_task(void) {
    const uint32_t interval_ms = 250;
    static uint32_t start_ms = 0;
    // Blink every interval ms
    if (board_millis() - start_ms < interval_ms) return; // not enough time
    start_ms += interval_ms;
    board_led_write(led_state);
    led_state -= 1; 
}
#define MAX_REPORT  4

static uint8_t const keycode2ascii[128][2] =  { HID_KEYCODE_TO_ASCII };

// Each HID instance can has multiple reports
static uint8_t _report_count[CFG_TUH_HID];
static tuh_hid_report_info_t _report_info_arr[CFG_TUH_HID][MAX_REPORT];

static void process_kbd_report(hid_keyboard_report_t const *report);
static void process_mouse_report(hid_mouse_report_t const * report);

//--------------------------------------------------------------------+
// TinyUSB Callbacks
//--------------------------------------------------------------------+
static struct
    {
    uint8_t report_count;
    tuh_hid_report_info_t report_info[MAX_REPORT];
    } hid_info[CFG_TUH_HID];

// Invoked when device with hid interface is mounted
// Report descriptor is also available for use. tuh_hid_parse_report_descriptor()
// can be used to parse common/simple enough descriptor.
void tuh_hid_mount_cb(uint8_t dev_addr, uint8_t instance, uint8_t const* desc_report, uint16_t desc_len)
{
    uint8_t const itf_protocol = tuh_hid_interface_protocol(dev_addr, instance);

    // printf("HID device address = %d, instance = %d is mounted\r\n", dev_addr, instance);

    // Interface protocol (hid_interface_protocol_enum_t)
    const char* protocol_str[] = { "None", "Keyboard", "Mouse" };
    // printf("HID Interface Protocol = %s\r\n", protocol_str[itf_protocol]);
    if ( itf_protocol == HID_ITF_PROTOCOL_KEYBOARD )
    {
        KeyBoard_Mounted = true;
    }

    // By default host stack will use activate boot protocol on supported interface.
    // Therefore for this simple example, we only need to parse generic report descriptor
    // (with built-in parser)
    if ( itf_protocol == HID_ITF_PROTOCOL_NONE )
        {
        hid_info[instance].report_count = tuh_hid_parse_report_descriptor(hid_info[instance].report_info,
            MAX_REPORT, desc_report, desc_len);

        // printf("HID has %u reports \r\n", hid_info[instance].report_count);

        }

    // request to receive report
    // tuh_hid_report_received_cb() will be invoked when report is available
    if ( !tuh_hid_receive_report(dev_addr, instance) )
        {

        // printf("Error: cannot request to receive report\r\n");

        }
    //if (interface_protocol == 1) KeyBoard_Mounted = true;
}

// Invoked when device with hid interface is un-mounted
void tuh_hid_umount_cb(uint8_t dev_addr, uint8_t instance)
{
    uint8_t const itf_protocol = tuh_hid_interface_protocol(dev_addr, instance);
    // printf("HID device address = %d, instance = %d is unmounted\r\n", dev_addr, instance);
    
    // const char* protocol_str[] = { "None", "Keyboard", "Mouse" };
    // printf("HID Interface Protocol = %s\r\n", protocol_str[itf_protocol]);
    if ( itf_protocol == HID_ITF_PROTOCOL_KEYBOARD )
    {
        KeyBoard_Mounted = false;
    }
}

// Invoked when received report from device via interrupt endpoint
void tuh_hid_report_received_cb(uint8_t dev_addr, uint8_t instance, uint8_t const* report, uint16_t len)
{
    uint8_t const itf_protocol = tuh_hid_interface_protocol(dev_addr, instance);

    if (itf_protocol == HID_ITF_PROTOCOL_KEYBOARD)
        {
        // printf("HID receive boot keyboard report\r\n");
        process_kbd_report( (hid_keyboard_report_t const*) report );
        }

    // continue to request to receive report
    if ( !tuh_hid_receive_report(dev_addr, instance) )
        {
        // printf("Error: cannot request to receive report\r\n");
        }
#if 0    
    (void) dev_addr;

    uint8_t const rpt_count = _report_count[instance];
    tuh_hid_report_info_t* rpt_info_arr = _report_info_arr[instance];
    tuh_hid_report_info_t* rpt_info = NULL;

    if ( rpt_count == 1 && rpt_info_arr[0].report_id == 0)
    {
        // Simple report without report ID as 1st byte
        rpt_info = &rpt_info_arr[0];
    }else
    {
        // Composite report, 1st byte is report ID, data starts from 2nd byte
        uint8_t const rpt_id = report[0];

        // Find report id in the arrray
        for(uint8_t i=0; i<rpt_count; i++)
        {
        if (rpt_id == rpt_info_arr[i].report_id )
        {
            rpt_info = &rpt_info_arr[i];
            break;
        }
        }

        report++;
        len--;
    }

    if (!rpt_info)
    {
        printf("Couldn't find the report info for this report !\r\n");
        return;
    }

    if ( rpt_info->usage_page == HID_USAGE_PAGE_DESKTOP )
    {
        switch (rpt_info->usage)
        {
        case HID_USAGE_DESKTOP_KEYBOARD:
            //TU_LOG1("HID receive keyboard report\r\n");
            // Assume keyboard follow boot report layout
            process_kbd_report( (hid_keyboard_report_t const*) report );
        break;

        case HID_USAGE_DESKTOP_MOUSE:  // no Mouse support
            //TU_LOG1("HID receive mouse report\r\n");
            // Assume mouse follow boot report layout
            //process_mouse_report( (hid_mouse_report_t const*) report );
        break;

        default: break;
        }
    }
#endif
}

//--------------------------------------------------------------------+
// Keyboard
//--------------------------------------------------------------------+

// look up new key in previous keys
static inline bool find_key_in_report(hid_keyboard_report_t const *report, uint8_t keycode)
{
    for(uint8_t i=0; i<6; i++)
    {
        if (report->keycode[i] == keycode) return true;
    }
    return false;
}
static inline void process_kbd_report(hid_keyboard_report_t const *p_new_report) 
{
    static hid_keyboard_report_t prev_report = {0, 0, {0}}; // previous report to check key released
    for (uint8_t i = 0; i < 6; i++)
    {
        if (p_new_report->keycode[i])
        {
            if (find_key_in_report(&prev_report, p_new_report->keycode[i])) {
                // exist in previous report means the current key is holding
            } else {    
                // not existed in previous report means the current key is pressed
                Key_Pressed(p_new_report->modifier & (KEYBOARD_MODIFIER_LEFTCTRL | KEYBOARD_MODIFIER_RIGHTCTRL),
                            p_new_report->modifier & (KEYBOARD_MODIFIER_LEFTSHIFT | KEYBOARD_MODIFIER_RIGHTSHIFT),
                            p_new_report->keycode[i]);
                uint8_t leds = keylock_flags;
                switch (p_new_report->keycode[i])
                {
                    case HID_KEY_NUM_LOCK:
                        leds ^= KEYBOARD_LED_NUMLOCK;
                        set_leds (leds);
                        break;
                    case HID_KEY_CAPS_LOCK:
                        leds ^= KEYBOARD_LED_CAPSLOCK;
                        set_leds (leds);
                        break;
                    case HID_KEY_SCROLL_LOCK:
                        leds ^= KEYBOARD_LED_SCROLLLOCK;
                        set_leds (leds);
                        break;
                    default:
                        break;
                }
            prev_report = *p_new_report;
            }
        }
    }
    prev_report = *p_new_report;
}

void set_leds (uint8_t leds)
{
    if ( leds != keylock_flags )
    {
    uint8_t const addr = 1;
    keylock_flags = leds;

    tusb_control_request_t ledreq = {
        .bmRequestType_bit.recipient = TUSB_REQ_RCPT_INTERFACE,
        .bmRequestType_bit.type = TUSB_REQ_TYPE_CLASS,
        .bmRequestType_bit.direction = TUSB_DIR_OUT,
        .bRequest = HID_REQ_CONTROL_SET_REPORT,
        .wValue = HID_REPORT_TYPE_OUTPUT << 8,
        .wIndex = 0,    // Interface number
        .wLength = sizeof (keylock_flags)
        };

    tuh_control_xfer (addr, &ledreq, &keylock_flags, NULL);
    }
}