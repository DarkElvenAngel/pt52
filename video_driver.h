#ifndef VIDEO_DRIVER_H
#define VIDEO_DRIVER_H

void video_init (void);
bool Blink_task(struct repeating_timer *t);

/**
 * Set Cursor Visiblity
 * 
 * \param visiblity on or off
 * \return none
 */
void Set_Cursor(bool visiblity);
#endif